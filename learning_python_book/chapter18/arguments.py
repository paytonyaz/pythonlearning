# -*- coding: utf-8 -*-
"""
Created on Sat Dec 26 11:16:18 2020

@author: wwilkinson
"""

# =============================================================================
# Example of changes of mutable objects
# =============================================================================

def changer(a,b):
    a = 2           #changes local name's value onlu
    b[0] = 'spam'   # changes shared object in place
    
x = 1
L = [1,2]
print('The original integer is:', x)
print('The original list is: ',L)
print('after calling the function "changer" these are now:')
changer(x,L)
print(x)    
print(L)
print('Note, how the list had an element changed, due to a "change in place" action')


# another example, this time with reassignment, thus not changing the higher level list
def changer2(a,b):
    a = 2           #changes local name's value onlu
    b = b + ['spam']   #a change, but with reassignment 
    
x = 1
L = [1,2]
print('The original integer is:', x)
print('The original list is: ',L)
print('after calling the function "changer" these are now:')
changer2(x,L)
print(x)    
print(L)
print('Note, the list was not altered because instead of a "change in place"',
      'a "reassignment" was used')

# =============================================================================
# Multiple Return Types
# =============================================================================

# this example displays how to return multiple objects from a function
# in reality, the function is returning just one object, a tuple, which is then
# unpacked when the assignement is called (in this case unpacked into newX and newL)
def multiple(x,y):
    x = 2
    y = [3,4]
    return x, y

x = 1
L = [1,2]
newX,newL = multiple(x, L)

# =============================================================================
# Argument passing
# =============================================================================
#there are multiple way to pass arguments in python

""" Method 1:
    By position:
"""
# this function takes in arguments by position and then prints them
def f(a,b,c):
    print(a,b,c)
f(1,2,3)

"""
Method 2
Keywords 
"""
# in this example, the same function is called, but now each name is given a specific value
f(c=3, b=2, a=1)

# one can combine the two methods
# first the positions are matched, then the names.
f(1,c=3, b=2)

""" 
Defaults
"""
# there is a way to specify default values, in case they are not passed in
def f2(a, b=2, c=3):    # a is required, b and c are defaulted to a specifc value
    print(a,b,c)

f2(1)   # uses default for b and c
f2(1,5) #just used default for c

# Note the difference between creating a function and calling a function
# when using the "a=3" syntact and CALLING A FUNCTION, this assigns the specific
# value to that variable name
# when using the "a=3" syntax in a FUNCTON DEFINITION, this sets up a default
# value for that argument.


# =============================================================================
# Arbitrary Arguments
# =============================================================================
#there are two ways to pass an arbitary number of arguments. 
#With the "*" and with the "**"

"""
Using the *
symbol
"""
#this function accepts an arbitary number of arguments
# this is done with the "*" indicator
def f3(*args):
    print(args)
    
#when the above function is called, python collects all the "arg"s into a tuple
# The tuple can then be indexed, stepped through with a loop or printed (like shown)
f3(3)
f3(1,2,3)
f3('hi')

"""
Using the **
symbol
"""
# the ** symbol only works with keyword assigments and packages them up in
# a dictionary

def f4(a, *pargs, **kargs):
    print(a, pargs, kargs)

f4(1,2,3, x=1, y=2)

# in the above example, f4 requires at least one argument, which is assigned to a
# all extra arguments (without a keyword) are collected in the pargs variable as a tuple
# all keyword variables are collected in kargs, as a dictionary

"""
These two ways to specifcy arbitrary arguments take on a different meaning when
used in function calls (instead of function definitions)
"""

def f5(a,b,c,d):
    print(a,b,c,d)

args = (1,2) #creates a tuple
args += (3,4)  #extends the tuple to 4 elements
f5(*args)   #calls the function f5, and unpacks the tuple args, to each of the arguments for the function

# in effect, using the * in a function call does the opposite as in a function definition
# it unpacks that variable into enough parts to correctly call that function

# Note that this only works if the number of items within the variable correctly match
# the function.
#If I extend args, and then call f5, it fails
args += (5,) #extends args to a 5th place
# f5(*args) #This fails

"""
Using ** in a function call
"""
#similar to *, the use of "**" unpacks dictionaries
args = {'a':1,'b':2,'c':3} #creates a 3 element dict
args['d'] = 4 #adds a fourth element

f5(**args) #calls the function, unpacks the dict

# these can be combined
f5(*(1,2), **{'c':3,'d':4})


# =============================================================================
# Keyword only arguments
# =============================================================================
# python syntax requires that keyword only arguments come after the "*args"
# in the argument list

# in this example, the function kw_only takes three arguments
# a is required and may be passed in by name or position
# *b collects all other positional arguments
# c must be a keyword argument (becuase it came after the *b argument)
def kw_only(a,*b,c):
    print(a,b,c)


kw_only(1, 2, 3, 4, c=5)
kw_only(1,c=3) #this works, since b does not need to be defined
kw_only(a=1,c=3)

# this will fail: kw_only(1,2,3) since c must be specified with a keyword


# a "*" can be used without a variable name, to indicate that all following variables
# must be specified with a keyword
#this function requires both b and c to be used with a keyword, since they come after the "*"
def kw_only2(a,*,b,c):
    print(a,b,c)

kw_only2(1,b=2, c=3)

#a modification of this function is to add defaults

def kw_only3(a,*,b=2,c=3):
    print(a,b,c)
    
kw_only3(1)
kw_only3(1,b=5,c=7)
kw_only3(1,b=5)

""" 
The addition of the **
"""
# keyword-only arguments must be specified after a single star (*), not the 
# double star (**)
# arguments cannot come after the double star (**), and the double star cannot be the
# only argument within a function definition.

#this fails: def bad(a, **, b, c):
# this also fails def bad(a, **kargs, b, c):
    

# this function has one variable which is positional (a) 
# *b collects all other positional variables
# c is a keyword only argument with a default value of 3
# all other keywords are collected with **kargs

def f6(a,*b, c=3, **kargs):
    print(a,b,c,kargs)

f6(1,2,4,c=5,d=3,f=1)
f6(1)

#using the unpacking ability of the * in a function call
args = (1,2)
args += (3,)
f6(*args)

#when calling functions, arguments must appear before the ** argument.
# example of calling f6 function, while specifying the unpacking of a dict.
f6(1,2,3, **dict(x=4,y=5))


# =============================================================================
# Chapter Exercise: Min Function
# =============================================================================
# Objective:
# To create a function that accepts an arbitrary number of arbitrary types of arguments
# and then returns the minimum value.

def min_func(*argin):
    min_val = argin[0]
    for element in argin[1:]: #this slices the argin, so the first one is not compared
        if element < min_val:
            min_val = element
    print('Min Value is: ',min_val)


min_func(1,2,3,4)

#there is another way
def min_func2(*argin):
    tmp = list(argin)  #converts the tuple to a list
    tmp.sort()  #sorts the list
    print('The min value is: ',tmp[0])
    
min_func2(1,2,-5,7,9)

# =============================================================================
# Chapter Exercise: Intersect Function:    
# =============================================================================
#Goal is to make an intersec function, similar to that doen in chapter 16
# but this time with the ability to take in an arbitrary number of arguments 

def intersect(*args):
    """
    Parameters
    ----------
    *args :at least two arguments, for which the similarities will be returned

    Returns
    -------
    elements commmon to all inputs

    Notes
    ------
    This was a function writen by me
    """
    res = [] #initilize result variable
    for char in args[0]:
        not_in = 0
        if char in res:
            continue  #prevents duplicates
        for string in args[1:]:  #this loops through all other tuples
            if char not in string:   #this checks to see if char is in the other tuple
                not_in = 1 #sets a variable to indicate a match didn't exist
        if not not_in:
            res.append(char)  #if matches are found in all strings, then add the char to the result
    return res


"""
Intersect2 is from the book
"""

def intersect2(*args):
    res = []
    for x in args[0]:
        if x in res: continue
        for other in args[1:]:
            if x not in other: break
        else:
            res.append(x)
    return res

#tests it out
intersect('scram','spam')    
intersect('spam','scrap','slam')

intersect2('scram','spam')    
intersect2('spam','scrap','slam')

# note the difference between the intersect and intersect2 functions
# intersect2 is more concise. This is done with an intelligent way of spreading an 
# if, else statement across a for loop.
# note how with the inner for loop, the if statement is under it, but the else statement
# is outside of it.

#one can also write it this way, with slighly different indenting, which shows the
# if statement differently
def intersect3(*args):
    res = []
    for x in args[0]:
        if x in res: continue
        for other in args[1:]:
            if x not in other:
                break
        else:
            res.append(x)
    return res

intersect3('spam','scrap','slam')

#Note that the intersect3 function (which is the same as instersect2, just indented differently)
#is only slightly different than intersect4, WHICH DOES NOT WORK.
# the slight difference is with that "else" within intersect3.
# in intersect3, when the break statement is executed, the operation returns to the
# top of the out loop
# in intersect4, when the break statment is executed, the inner loop is broken,
# but the res.append(x) statement is still executed.

def intersect4(*args):
    """
    this doesn't do what it should

    Parameters
    ----------
    *args : TYPE
        DESCRIPTION.

    Returns
    -------
    res : TYPE
        DESCRIPTION.

    """
    res = []
    for x in args[0]:
        if x in res: continue
        for other in args[1:]:
            if x not in other:
                break
        res.append(x)
    return res

intersect4('spam','slam','scrap')