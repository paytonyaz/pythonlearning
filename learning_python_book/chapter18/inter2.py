# -*- coding: utf-8 -*-
"""
Created on Wed Dec 30 11:23:09 2020

@author: wwilkinson

This module contains three functions: intersect, union and a tester function to
test them all out
"""

def intersect(*args):
    """
    Parameters
    ----------
    *args :at least two arguments, for which the similarities will be returned

    Returns
    -------
    elements commmon to all inputs

    Notes
    ------
    This was a function writen by me
    """
    res = [] #initilize result variable
    for char in args[0]:
        not_in = 0
        if char in res:
            continue  #prevents duplicates
        for string in args[1:]:  #this loops through all other tuples
            if char not in string:   #this checks to see if char is in the other tuple
                not_in = 1 #sets a variable to indicate a match didn't exist
        if not not_in:
            res.append(char)  #if matches are found in all strings, then add the char to the result
    return res

def union(*args):
    """
    This will return the elements of all passed squences

    Parameters
    ----------
    *args : TYPE
        DESCRIPTION.

    Returns
    -------
    None.

    """
    res = []
    for string in args:
        for char in string:
            if char in res: 
                continue
            else:
                res.append(char)
    return res


def tester(func, items, trace = True):
    """
    This will run a called function with a different order of arguments
    This will deteremine if the output depends on the order of the arguments
    """
    for ii in range(len(items)):
        items = items[1:] + items[:1] #reorders the items
        if trace:
            print(items)
        print(sorted(func(*items))) #calls the function, unpacking the arguments
 
#test out tester with a call to intersect
tester(intersect,('abcdefg','ab','aaabbb','abcd'))



