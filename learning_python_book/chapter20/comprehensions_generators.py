# -*- coding: utf-8 -*-
"""
Created on Tue Jan 26 19:32:45 2021

@author: wwilkinson
"""

s1 = 'this is a string'
ascii_vals = [ord(x) for x in s1]
print('The ascii char within the string:')
print(s1)
print('are:')
print(ascii_vals)

#if we want to instead save it in hex:
ascii_vals_hex = [hex(ord(x)) for x in s1]
print('these values in hex are:')
print(ascii_vals_hex)
   

# =============================================================================
# Comparing Mapping and List comprehension
# =============================================================================

#many times list comprehensions and map can do the same thing
#for instance, the code shown above can be done with a mapping function
ascii_vals_map = list(map(ord,s1))

#many times list comprehension will be easier, as it is more general. You
# can use an expression for list comprehension, whereas a map call require
# a function to be passed, thus may require creating a function itself.

#also, list comprehension can be more flexibile, they have the ability to 
# have "if" clauses added to them, essentially mimicking the filter function too

#as an example. This picks out even numbers from 0 to 4
[x for x in range(5) if x%2 == 0]

#the filter equivalent is:
list(filter(lambda x: x%2 == 0, range(5)))

#in the case shown above, the filter call is not much longer than the list comprehension
#one, however, the list comp. one can be extended.
#if we wanted for instance to square, just the even numbers from 0-4
[x ** 2 for x in range(5) if x%2 == 0] #this returns [0, 4, 16]
#the line shown above is essententially a combination of a map and a fitler function.

#to do this with maps and filters you will have to do this:
list(map(lambda x: x**2, filter(lambda x: x%2 == 0, range(5))))
#which is pretty complicated.


#list comprehension can be even more general, in that they can have mulitple if 
# and loop cases
# the general structure is:
# [expression for target in iterable condition]
#but this can be extended
#[expression    for target1 in iterable1 condition1]
#               for target2 in iterable2 condition2
#               for targetN in iterableN conditionN]

#for example:
print([x + y for x in [1,2,3] for y in [100,200,300]])
#is the same as:
res = []
for x in [1,2,3]:
    for y in [100,200,300]:
        res.append(x+y)
print(res)


#each for loop can have its own if clause within a list comprehension
[(x,y) for x in range(5) if x%2==0 for y in range(5) if y%2 ==1]

"""
List Comprehensions and Matrixs
"""
#one basic way to code matrices is with nested list statements
M = [[1,2,3],
     [4,5,6],
     [7,8,9]]

N = [[2,2,2],
     [3,3,3],
     [4,4,4]]

#with this structure we can index rows and columns
print(M[1]) #prints the 2nd row, [4,5,6] of M
print(M[1][2]) #prints the 3rd element of the 2nd row [6]

#list comprehensions are great for traversing matrices.

#for instance if we wanted the 2nd column of M
[row[1] for row in M]

#if we wanted to pick out a diagnol
[M[i][i] for i in range(len(M))] #returns [1,5,9]
#or if we want the other diagnol
[M[i][len(M)-1-i] for i in range(len(M))] #return [3,5,7]


#two different ways of adding 10 to each element in M
#these two implementations are subtly different.
[col+10 for row in M for col in row]
#the first statment uses two loops. The first loop created (the row in M) is the 
#outer loop, and then the 2nd loop (the col in row) is the inner loop
#this creates on list, with 10 added to each element in the array M

[[col+10 for col in row] for row in M]
#for this second statement, things are different. The end result is
# [[11,12,13],
 # [14,15,16],
 # [17,18,19]]
#this recreates the structure of M.
# This is equivalent to the following for loop
res = []
for row in M:
    tmp = []
    for col in row:
        tmp.append(col+10)
    res.append(tmp)
#the brackets to the left, is a nested expresssion., So the first loop (the outer loop)
#is actually the rightmost loop in the statement (row in M), for which the 
# expression in brackets to the right is evaluated

#list comprehension can also be used to manipulate elements in multiple matrices
#for instance to perform an element by element multiplication of M and N
[M[row][col]*N[row][col] for row in range(3) for col in range(3)] #this creates a flat vector
#to recreate the shape of the original matrix, use the nesting shown in the previous example
[[M[row][col]*N[row][col] for col in range(3)] for row in range(3)]
#this previous line works because the row iteration forms the outer loop.

"""
keep it simple stupid
"""
#in the real world, complex list comprehension statements should not be used.
#they can significantly reduce the readability of the code.

# =============================================================================
# More list comprensions
# =============================================================================
#this assumes there is a myfile.txt already in the directory

print(open('myfile.txt').readlines())

#the "\n" can be removed with the help of list comprehension
print([line.rstrip() for line in open('myfile.txt').readlines()])

#of course the file object itself is iterable, so we do not need th readlines() method
print([line.rstrip() for line in open('myfile.txt')])

#this can also be done with a mapping call
print(list(map(lambda line: line.rstrip(), open('myfile.txt'))))

#scanning through lists can be useful when importing data from databases.
#suppose a SQL database provided the following data:
listoftuples = [('bob',35,'mgr'), ('sue', 40, 'dev')]
#the age information can be pulle dout easily with list comprehension
[age for (name, age, job) in listoftuples]

#this can alos be done with map
list(map(lambda row: row[1], listoftuples))


# =============================================================================
# Generator Functions and Expressions
# =============================================================================
#there are two types of generators
#   Generator functions
#   Generator expressions
#Both of these delay object creation, until it is needed. Also, they only
# produce the output that is needed at that time
#They do this by implementing the iteration protocol shown in chapter 14

#this code creates a generate the creates the quares of a series of numbers
def gensquare(N):
    for i in range(N):
        yield i**2 #squares value. The yield is the point the geneerator returns to
        
#When the function (or generator) yields a value, it returns to its caller each time
#when it is resumed, the prior state is restoredm and control pucks up agian immediatley after
# the yield statment.

# the gensquare generator can be used within a for loop like so. This will return
#  0 : 1 : 4 : 9 : 16 : 
for a in gensquare(5):
    print(a, end=' : ')

#to see what is really going on
x = gensquare(4) #this creates a generator function
print(type(x)) #x is a generator. Which is an iterator, meaning it has the __next__ method
#prints the inbuilt methods
print([y for y in dir(x) if y.startswith('__')])

next(x)
next(x)
next(x)
next(x)
next(x) #this generates the StopIteration 

"""
Why Generators?
"""
#with the simple examples shown, it is not obvious why one would use generator.
# but they are often more efficient in terms of memory and performance when
# a lot of computation is required to generate each value
#Generators distribute the time required to produce the series of values
# of values over loop iterations.

#generates can be paired with comprehensions
#creates a generator to capitalize all items, separated by a comma
def ups(line):
    for item in line.split(','):
        yield item.upper() #capitilizes the value

print(tuple(ups('aaa,bbb,ccc')))

#this can then be used in a comprehension
{i: s for (i,s) in enumerate(ups('aaa,bbb,ccc'))}

#of course this doesn't need to be done with generators. Below is the same function
# without using generators
aa = 'aaa,bbb,ccc'
print({i:s for (i,s) in enumerate(aa.upper().split(','))})


# =============================================================================
# Generator expressions
# =============================================================================
#generator expressions share the same syntax as list comprehensions
# but are inclosed in round brackets, not square

#list comprehension:
[x**2 for x in range(4)]

#generator expression
(x**2 for x in range(4))

G = (x**2 for x in range(4)) #creates a generator object
next(G)
next(G)
next(G)
next(G)
#typically we don't call this manually, and use it in a loop
for num in (x**2 for x in range(4)):
    print('{}, {}'.format(num, num/2.0))

#much like generators, generator expressions are handy for memory use. Now instead
# of creating the whole item at once it is broke into pieces.

#for example the following shows the list comprehension and generator expression 
#implementation
line = 'aaa,bbb,ccc'
#list comprehension
''.join([item.upper() for item in line.split(',')])

#generator expression
''.join(item.upper() for item in line.split(','))
#the list comprehension version must create the whole list before joining everything
# the generator expression bypasses this, and just works on the iterable itself.

# =============================================================================
# Using generators
# =============================================================================
#as a directory walker
# os.walk is a generator, which is iterable
import os
for (root, subs, files) in os.walk('.'):
    for name in files:
        print(root, name)


# =============================================================================
# Chapter Examples:
# =============================================================================
"""
Scrambling Sequencies
with loops
"""
# the following code was shown in Chapter 18
L, S = [1,2,3], 'spam' #defines a list and a string

for ii in range(len(S)):
    S = S[1:] + S[:1] #moves first element to the end
    print(S, end=' ')

for ii in range(len(L)):
    L = L[1:] + L[:1]
    print(L, end= ' ')

"""
Scrambling with functions
"""

#The code shown above only works on specifically named variables. We can 
#make it more general with a function

def scramble(seq):
    res = []
    for ii in range(len(seq)):
        res.append(seq[ii:]+seq[:ii]) #shifts the order around
    return res

scramble('spam')

# we can define the function more concisely
def scramble2(seq):
    return [seq[i:]+seq[:i] for i in range(len(seq))]
    
scramble2('spam')

# and we can loop these function
for x in scramble((1,2,3)):
    print(x, end=' ' )

"""
Scrambling with Generator functions
"""
#all those implementations shown above work, but must build the entire result
# list in memory at once. This results in the caller waiting for the entire
#list to be complete
#instead we can use generators 
def scramble3(seq):
    for i in range(len(seq)):
        seq = seq[1:] + seq[:1]
        yield seq

#doing it the other way (and making one line shorter)

def scramble4(seq):
    for i in range(len(seq)):
        yield seq[i:] + seq[:i]
        
#then wrap the results in a list to show (also wrap in a print to print)
print(list(scramble3('spam')))
print(list(scramble4('spam')))

"""
Scrambling with Gen Expressions
"""
#next we can use generator expressions
S = 'spam'
G = (S[i:]+S[:i] for i in range(len(S)))
print(list(G))

#%% Trying Escobars thing
def foo(func):
    def wrap():
        print('before Func')
        func()
        print('After func')
        
    return wrap

@foo
def bar():
    print('my bar function')
    
#bar = foo(bar) #no longer need this after using the @symbol
#%% Seeing the advantages of generators.

#the permute.py script contains two permutes functions.
    #permute1 is a function
    #permute2 is a generator
#now we will see the advantage of using generators.
#permutations expand by the factorial. in other words they get stupid big.
#for a list of 10 items the number of permuations is 10!
import math
print('The numbe of permuations for a 10 item list is:')
print(math.factorial(10))
print('A nicer way of printing this is:')
print('{:,d} permuations'.format(math.factorial(10)))

from permute import permute1, permute2
seq = list(range(10)) #creates a sequence of 10 items
p1 = permute1(seq) #this will ask the computer to generate all 3million combinations at once

#prints out the length and first 2 values 
print(len(p1), p1[0], p1[1])

#now when using generators, the answer is nearly immediate, becuase only the first item is generate
p2 = permute2(seq) # returns the generator immediately
print(next(p2)) # produces the first result very quickly 
print(next(p2))  #second result

#this difference becomes even more stark with longer items. If the sequence is 50 char
# long, building a results list becomes impossible.
print('The number of permuations for 50 char list is:')
print('{:,d}'.format(math.factorial(50)))
#but a generator can still return the results on at a time
p50 = permute2(list(range(50))) #creates generator
print(next(p50)) #first permutation for a 50 item list
print(next(p50)) #second permuation

#%% Creating a home made map function
#the map function within python can be recreated

def mymap(func, *seq):
    res = []
    for args in zip(*seq):
        res.append(func(*args))
    return res

#print(mymap(abs,[-2,-1,1,2,3]))
print(mymap(pow,[1,2,3],[2,3,4]))

#%% Other types of Comprehensions
#in addition to list comprehensions and generators, there are also comprehensions
# for dictionaries and sets

aa = dict(a=1,b=2,c=3)

bb = [aa[item] for item in aa]