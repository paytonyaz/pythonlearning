# -*- coding: utf-8 -*-
"""
Created on Wed Feb 10 21:49:10 2021

@author: wwilkinson
"""

#generator function
def scramble(seq):
    for i in range(len(seq)):
        yield seq[i:] + seq[:i] #yield on item per iteration
        
