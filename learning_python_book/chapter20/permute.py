#permute.py for chapter 20 exercise
def permute1(seq):
    # print('Incoming seq is:', seq)
    if not seq:
        # print('\t\t\treturning: ', [seq])
        return [seq]
    else:
        array = []
        for i in range(len(seq)):
            # print('\ti = ', i)
            rest = seq[:i] + seq[i+1:] #delete current node
            # print('\tTotal Sequence is: ',seq)
            # print('\tremaining seq is: ', rest)
            for x in permute1(rest):
                array.append(seq[i:i+1]+x) #add node at front
                # print('\t\t x = ', x, end = ', ')
                # print('')
                # print('\t\tAdding: ',seq[i:i+1],' to x')
                # print('\t\t array = ',array)
        return array

def permute_bill(seq):
    if not seq:
        return [seq] #note, this doesn't work with []. It has to be a "near" empty list
    else:
        array= []
        for index in range(len(seq)):
            goose = seq[index] #removes a value
            remain = seq[:index] + seq[index+1:] #everything but goose
            for x in permute_bill(remain):
                array.append(goose + x)
    return array

def permute2(seq):  #this is the generator version of the recursive call
    if not seq:
        yield seq
    else:
        for i in range(len(seq)):
            rest = seq[:i] + seq[i+1:]
            for x in permute2(rest):
                yield seq[i:i+1] + x
