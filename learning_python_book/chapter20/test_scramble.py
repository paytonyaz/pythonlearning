# -*- coding: utf-8 -*-
"""
Created on Wed Feb 10 21:50:35 2021

@author: wwilkinson
"""

#this script is designed to test the scrambling exercise
from scramble import scramble
from inter2 import intersect, union #these are functions made in chapter 18

#this function will call a function with scrambled inputs
#idea is to ensure the response is the same even if the order differs.
def tester(func, items, trace=True):
    for args in scramble(items):
        if trace: print(args)
        print(sorted(func(*args)))
        
tester(intersect, ('aab','abcde','ababab'))

#this finds that 2 is the only common item
tester(intersect,([1,2],[2,3,4],[1,6,2,7,3]),False)
#this will print out all the different combinations tested
tester(intersect,([1,2],[2,3,4],[1,6,2,7,3]))

from permute import permute1, permute_bill

# count = 0
# a = 'ab'
# permute1(a)
# # a = 'abc'
# # permute1(a) #this prints ['abc', 'acb', 'bac', 'bca', 'cab', 'cba']
# count = 0
# permute1('abc')

permute_bill('abc')

# permute1('abc')
