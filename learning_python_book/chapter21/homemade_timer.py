# -*- coding: utf-8 -*-
"""
Created on Sat Apr  3 13:57:14 2021

@author: wwilkinson
Homegrown timing tools for function calls
Does total , best-of time, and best of totals time
"""
import time, sys
# note: the "timer" variable isn't a number but reassigns the function time.clock
# this is different than what was used in the timer0.py script. The difference
# is in the parenthesis: 
#   aa = timer.clock() -> this creates a number, from timer.clock() function
#   bb = timer.clock  -> This creates a function bb, which does the same as calling timer.clock()
timer = time.clock if sys.platform[:3] == 'win' else time.time

def total(reps, func, *pargs, **kargs):
    """Total time to run func(), reps, number of times
    Returns (total time, last result)
    """
    repslist = list(range(reps))
    start = timer()
    for ii in repslist:
        ret = func(*pargs, **kargs)
    elapsed = timer() - start
    return (elapsed, ret)

def bestof(reps, func, *pargs, **kargs):
    """
    Quickest func() among all reps run
    Returns: (best time, last result)
    """
    for ii in range(reps):
        start = timer()
        ret = func(*pargs, **kargs)
        elapsed = timer() - start
        # assigs best to the first value obtained
        if ii < 2:
            best = elapsed
        elif elapsed < best: 
            best = elapsed
    return(best, ret)

def bestoftotal(reps1, reps2, func, *pargs, **kargs):
    """
    This will call the two above functions
    reps1 = number of outer loops
    reps2 = numbe ro finner loops
    """
    return bestof(reps1, total, reps2, func, *pargs, **kargs)


