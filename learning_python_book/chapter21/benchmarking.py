# -*- coding: utf-8 -*-
"""
Created on Thu Apr  1 19:45:55 2021

@author: Bill
"""
from timer0 import timer0 

timer0(pow,2,1000)#will call 2^1000 1000 times

timer0(str.upper, 'spam')  #calls 'spam'.upper() 1000 times




#%%
#defining a function to print results nicely
def print_time_results(time,last_result):
    template = '{0:.2e}s of total time, {1} as last return'.format(time, last_result)
    print(template)

#%%

"""
Now we will use the "homegrown" timer function within the homemade_timer.py module
"""
import homemade_timer as hm_timer #this imports all 3 function from homemade_timer.py

hm_timer.total(1000, pow, 2, 8)[0] #this returns the first element of the return value (the total time)
#'{0:.3f}s of total time, {1:.2f} as last return'.format(hm_timer.total(1000, pow, 2, 8))
power_time = hm_timer.total(1000, pow, 2, 8)
'{0:.2e}s of total time, {1:.2f} as last return'.format(*power_time)
print(hm_timer.total(1000, pow, 2, 8)) #prints both return values

print_time_results(*power_time)

best_of_test = hm_timer.bestof(1000, pow, 2, 10)
print_time_results(*best_of_test)

best_of_total_test = hm_timer.bestoftotal(100, 1000, pow, 2, 12)
print_time_results(*best_of_total_test)

time_to_cap =hm_timer.bestoftotal(50, 1000, str.upper, 'spam')
time_to_square =hm_timer.bestoftotal(50, 1000, pow,1800,2)

print_time_results(*time_to_cap)

print_time_results(*time_to_square)


