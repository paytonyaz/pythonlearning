# -*- coding: utf-8 -*-
"""
Created on Thu Apr  1 19:48:05 2021

@author: Bill
"""
import time

#this is a simple timer function, which works but has some issues
#  * Doesn't support keyword arguments in the tested function call
#  * harcodes the repitition count
#  * Uses the timer.clock function, which may not work on systems other than windows
# * Does't let the user know the funciton actually worked
# * Only gives total time. 
# To fix these shortomcings a new timer function will be called
def timer0(func, *args):
    start = time.clock() #starts the timer
    for i in range(1000):  #executes some function 1000 times
        func(*args)
    #print(time.clock() - start)
    return time.clock() - start #total elapsed time

