# -*- coding: utf-8 -*-
"""
Created on Sun Apr 25 12:57:39 2021
Test the relative speed of iteration tool atlernatives
@author: wwilkinson
"""

import sys, homemade_timer

reps = 10000
repslist = range(round(-1*reps/2),round(reps/2))

#%% Defines function to test various ways of incrementing through things
def forLoop():
    res = []
    for ii in repslist:
        res.append(abs(ii))
    return res

def listComp():
    return [abs(x) for x in repslist]

def mapCall():
    return list(map(abs,repslist))

def genExp():
    return list(abs(x) for x in repslist)

def genFunc():
    def gen():
        for x in repslist:
            yield abs(x)
    return list(gen())

#%% Now operates on those functions
print(sys.version)
for test in (forLoop, listComp, mapCall, genExp, genFunc): 
    (bestof, (total, result)) = homemade_timer.bestoftotal(5, 1000, test)
    #print('%-9s: %.5f => [%s...%s]' % test.__name__, bestof, result[0], result[-1]))
    print('{0}: {1:.5f}s => [{2} .. {3}]'.format(test.__name__,bestof,result[0], result[-1]))
    