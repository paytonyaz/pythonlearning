# -*- coding: utf-8 -*-
"""
Created on Thu Dec 31 11:02:40 2020

@author: wwilkinson
"""

# =============================================================================
# Recursion
# =============================================================================
#recursion works in python, although is often overkill.
#for instance, the following function sums a list.

def recur_sum(L):
    if not L:
        return 0
    else:
        return L[0] + recur_sum(L[1:]) #recursive call, with the first element removed
    
    
L = [1,2,3]
print('The sum of all elements in', L, 'is:')
print(recur_sum(L))

# that is a bit overkill, and can easily be done in a loop
def loop_sum(L):
    sum = 0
    for element in L:
        sum += element
    return sum

print(loop_sum(L))

#but for some odd shaped objects, recursion is useful.
#for instance for arbitrarily shaped objects, such as this:
L = [1, [2, [3, 4] ,5], 6, [ 7, 8 ] ] #this is a very deep list

#the following function loops through each element of the list
# it then checks to see if that element is a list, or just a number (since the list can be arbitrarily deep)
# if it is just a number, it adds it to the sum
# if it is another list, it calls itself.
# this is an example of how recursion can traverse randomly shaped data structures

def sum_tree(L):
    sum = 0
    for x in L:
        if not isinstance(x,list): #this checks to see if the element is a single element or a list
            sum += x
        else:
            sum += sum_tree(x)
    return sum

print('The sum of all elements within', L, 'is:')
print(sum_tree(L))        

# =============================================================================
# Using "stacks" to traverse items
# =============================================================================
# Python implements recursion by pushing information on a call stack at 
# each recursive call, so it remembers where it mus return to. It is generally
# possible to implement recursive style procedures without recurisve calls, by using
# an explicit stack or queue to keep track of the remaining steps.

#this function performs the same as the above but without a recursive call
def sum_tree2(L):
    total = 0
    item = list(L) #ensure it is a list
    while item:
        front = item.pop(0) #removes the first element of the list
        if not isinstance(front,list): #checks to see if the item is a list
            total += front #if it isn't (it is an int) then it adds to the sum
        else:
            item.extend(front)  #if it is a list, it adds the elements of the front back into the orignal list
            # this essentially "flattens" the initial list
    return total

L = [1, [2, [3, 4] ,5], 6, [ 7, 8 ] ] #this is a very deep list
print(sum_tree2(L))

# =============================================================================
# Function Objects
# =============================================================================
#As with much in python, functions are themselves objects, and can be passed
# around as such

def echo(message):
    print(message)

# this is a direct call to the function
echo('Direct Call')

x = echo #now, the function is being reassigned
x('Indirect call')

#since functions are objects, they can be passed into other functions, just like normal objects
#below is an example
def indirect(func, arg):
    func(arg) #calls the function passed in
    
indirect(echo,'string') #this is actually calling a function, which calls another function.

#functions can also be emdeded within data structures
schedule = [(echo,'spam'), (echo, 'Ham')] #this creates a list of tuples, which contain a function and a string
#then we an increment through them
for (func, arg) in schedule:
    func(arg)
    
# we saw exampls of closure functions in chapter 17
# this is afuncton which defines another function, with a specific label
def maker(label):
    def echo2(message):
        print(label + ':' + message)
    return echo2 
       
F = maker('this is a label')
F('This is a message')
F('Now this will always print: "this is a label" before the second string')

# =============================================================================
# Function Inspection
# =============================================================================
#Functions can also be inspected

#basic function
def func(a):
    b = 'spam'
    return a*b

func(8) #this returns spam, 8 times

#but the function can also be inspected
print(func.__name__) #this returns the name of the function
print(dir(func)) #this returns all the attributes of the function

#these attributes also have attributes
print(dir(func.__code__)) #prints the attributes of the code attribute of the function
#some of these are useful. For instance, you can inspect the variables within the function
print(func.__code__.co_varnames) #this prints the variable names within the function
print(func.__code__.co_argcount) #this prints the number of arguments the function takes

#we can also create attributes and attach them to the function
func.count = 0 #creates an attribute call count
func.count += 1 #now increments it
func.handles = 'Button-Press' #creates another arbitrary attribute.
print(dir(func)) #and now those show up.
#all internally generated attributes have "__" in them. This was done to separate them
# from user generated attributes.

#you can loop through and search out user generated attributes
[x for x in dir(func) if not x.startswith('__')] #prints out all user generated attributes

# =============================================================================
# Function Annotations
# =============================================================================

#the following provides annotations to a function.
# for inputs these are deliniated by a ":" following the argument
# for the output, it is precededed ny a "->"
def func2(a:'first Arg', b:(1,10), c:int) -> int:
    return a+b+c

#annotations show up as another attribute within the function
func2(1,2,3)    
print(func2.__annotations__) #this returns a dict
# we can print all these
for arg in func2.__annotations__:
    #remember when looping through dicts, we get the keys.
    print(arg, '=>', func2.__annotations__[arg])

#not every argument needs to have an annotation
def func3(a, b:(1,15), c: int):
    return a+b+c
print(func3.__annotations__)

#when combining annotations with defaults, it looks like this
#Note that spyder code view does not like this, and highlights it as an error, although it works
def func4(a:'first'=2, b:(1,10)=5,c:int=2):
    return a+b+c
print(func4.__annotations__)
#the default value is specified after the annotation.

# =============================================================================
# Lamda
# =============================================================================
#lambda is an inline function expression. It can be handy to quickly make a function
# also, since it is an expression, and not a statement (stadard function defs are statements)
# it can be used in places a typical def cannot be used, such as in arguments or in lists

#below is an example of a landa expression that does the same as the preceding function def
def simpleFunc(a,b,c):
    return a+b+c

simpleFunc_L = lambda x,y,z: x+y+z

#both of these report the same thing
print(simpleFunc(1,2,3))
print(simpleFunc_L(1,2,3))


#lambdas are written like so
#lamda arg1, arg2, arg3... : expression using arguments

#defaults work on lambda's too
x = lambda a='fee',b='foe',c='fum': a+b+c
print(x()) #this returns feefoefum, the default from the lambda expression

#lamdas use the same type of scope as normal defs
def preferedTitle(title):
    action = lambda x: title + ' ' + x
    return action

knight = preferedTitle('sir')
#these will return "sir robin" and "sir lacelot"
knight('robin')
knight('Lancelot')

#this will return "Lady Margret"
lady = preferedTitle('Lady')
lady('Margret')

#lamdas can be handy to stuff short functions into lists
#this makes a list of 3 short functions, which can be called easily
L = [lambda x: x**2,
     lambda x: x**3,
     lambda x: x**4] #inline function def

for f in L:
    print(f(2)) #prints 4,8,16
    
print(L[0](2)) #prints 4

#The same thing can be done with def statements, but requires dedicated function names
# which may clash with other names, so using lambda is easier

#The same thing can be doen with dictionaries to make switch/branches

D = {'first': lambda: 2*1,
     'second': lambda: 2*2,
     'third': lambda: 2*3}

key = 'second'
print(D[key]()) #this prints 4 (note, the use of parethesis to make it a function call)


# =============================================================================
# Mapping Functions over Iterables: Map
# =============================================================================
#we often want to perform a common action to all items within an iterable
# the map function applies a passed-in function to each item within an iterable object,
# and returns a list containing all the function call resulst.

#for examples, lets say we want to add to to each item in this array
counter = [1,2,3,4]

#first we define the function
def inc(x): 
    return x+10
#then we apply map
ten_added = map(inc,counter) #however, this returns as a "map" object
#we have to inclose it in a list call to make it useable
print(list(ten_added))

#because map accepts a function, it is common to use a lambda here.
print(list(map(lambda x: x+3,counter))) #this adds three to each item


#map can be called with multiple sequences, which then it runs in parallel. 
# for instance. the pow function raises on arguiment to the other
pow(2,3) #2**3
print(list(map(pow,[1,2,3],[3,2,1]))) #returns 1**3, 2**2, 3**1
#with multiple sequences map expects an N argument function with N sequences
# the pow function expects two inputs, thus 2 lists must be provided.

# =============================================================================
# Filter
# =============================================================================
#filter selects an iterables item based on a test function
list(range(-5,5)) #this creates a list from -5 to +4
#we can filter this, by using the filter function.
#here only values greater than 0 are returned
list(filter(lambda x: x>0, range(-5,5)))

# =============================================================================
# Reduce
# =============================================================================
#reduce is similar to filter, but a little more complicated. It assumes
# two variables being passed in
from functools import reduce
reduce(lambda x,y: x+y, [1,2,3,4]) #this returns the sum of the list
#this way reduce works, is at each item is passes the current sum along wiht the
# next item.
#here is the for loop equivelent
L = [1,2,3,4]
result = L[0]
for x in L[1:]:
    result = result + x
print(result)    

# we can also make our own version of reduce
def my_reduce(func, sequence):
    tally = sequence[0]
    for next_item in sequence[1:]:
        tally = func(tally,next_item)
    return tally

my_reduce(lambda x,y: x+y,L) #prints 10, the sum of the list