# -*- coding: utf-8 -*-
"""
Created on Fri Jan 15 19:33:36 2021

@author: wwilkinson
"""

#Goal is to practice calling functions

from advanced_functions import sum_tree

list1 = [1,2,3,4,5]

sum1 =  sum_tree(list1)

#it is interesting that when calling sum_tree, that the console displays
# The sum of all elements in [1, 2, 3] is:
# 6
# 6
# The sum of all elements within [1, [2, [3, 4], 5], 6, [7, 8]] is:
    
# This is because these are the non-function lines within the file "advanced_functions"
# So when importing a function from a module, that whole modules code (that isn't within a function) is run.
# very good to know.

def recursive_sum(L):
    total_sum = 0 #intilizes
    for item in L:
        if type(item) == int:
            total_sum+=item
        else:
             total_sum+=recursive_sum(item)
    return total_sum


L = [10,11,[12,13],14,15]
recursive_sum(L)        


