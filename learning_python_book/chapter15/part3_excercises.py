# -*- coding: utf-8 -*-
"""
Created on Sat Oct 24 14:47:54 2020

@author: wwilkinson
"""

"""
Question 1
"""
#Make a for loop to rpint out the ASCII code for each char in a string.

S = 'this is a string'
for char in S:
    print(char, '=', ord(char))

# now change it to compute the sum of all the ASII char
total_val =0
for char in S:
    total_val += ord(char)
print('Sum of all ASCII codes in "',S,'" is: ', total_val)


#create a list that contains all the ASCII codes
asc_codes = [ord(x) for x in S]

"""
Question 2
"""
# Backslash char
#what happens with:

for ii in range(50):
    print('hello %d\n\a' % ii,end='')
# the \a is a "bell char". That should beep 50 times... seems like the looping
# in spyder prevents this


"""
Question 3
"""
#write a for loop that prints out a dictionaries items in sorted order.

D = {'a': 5, 'b': 7, 'coffee': 10, 'apples': 35}
# this question is ambigious. Does he want the values or keys sorted.
#to show the sorted keys we can do this.
sorted_d = sorted(D)
for keys in sorted_d:
    print(keys, D[keys])

# to print the sorted values we must be trickier
sorted_d = sorted(D.items(),key=lambda x: x[1])
for (key,val) in sorted_d:
    print(key, val)
    
"""
Question 4
"""
L = [2 **x for x in range(9)]  
# print out the index for which a lists element equals 32 (2^5)

[print(index) for (index,val) in enumerate(L) if val == 32]

#another way of doing this
if 32 in L:
    print('This occurs at index', L.index(32))
else: 
    print('Value not found')
    


    

