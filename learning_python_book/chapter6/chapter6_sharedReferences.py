# -*- coding: utf-8 -*-
"""
Created on Sat Aug 15 12:55:15 2020

@author: wwilkinson
"""


# =============================================================================
# All variables reference an object. The objec them controls the behvior 
# =============================================================================

# THis has an impact when using mutable data types, such as list.
L1 = [1,2,3];  
L2 = L1;   # L2 shares the same reference as L1.

print("L1 = ",L1, "L2 = ", L2)

L1 = 24;  # now L1 changes it references to an integer obj. L2's reference
# which pointed to a list object is still intact
print("L1 was no declared as an integer, = 24")
print("L1 = ", L1, "L2 = ", L2)

# but things get tricky if a value changes "in place" for instance
L1 = [1,2,3];
L2 = L1;
print('Now we are back to where we where before:\nL1 = ',L1,'L2 = ',L2)
# now if I change an ELEMENT within L1:
L1[0] = 24;
print('First element of L1 was changed to 24')
print('L1 = ',L1,'L2 = ', L2)
print('Notice how BOTH L1 AND L2 CHANGED!')
print('This is because they both reference the same list object')

print('This can be changed, by COPYING the list')
print('There are multiple ways of doing this, but for lists, it is easiest to slice')
L3 = [4,5,6]
L4 = L3[:]; # forces a copy of L3, not a shared reference
L3[0] = 24;
print('L3 = ', L3, 'L4 = ', L4)

# =============================================================================
# Shared references and Equality
# =============================================================================
# first let's create a shared reference:
L = [1,2,3]
M = L;
#then check equality
print('Are L and M equal?\n',L==M) # this is the most basic test, which checks 
# the contents for quality
print('Do L and M share the same object?\n', L is M) # this checks the objects. 

# for instance:
L = [1,2,3];
M = [1,2,3]; 
# they now have the same values, but do not share the same exact referenced object
print('are L and M the same value?\n',L==M)
print('Do L and M point to the same object?\n', L is M)
# in this case, the 2nd one is false.

# however, this does not work the same for simple integers, because Python 
# caches them
X = 42;
Y = 42
print('X and Y are:\t', X,'\t', Y)
print('Are X and Y equal?\n', X==Y)
print('Do X and Y point to the same object?\n',X is Y)
# not how both of the X,Y comparisons are true, even though we didn't explicitly
#set the two objects to reference each other. This is due to python caching simple
# integer values. It does the same for simple strings

# You can determine the number of references by using getrefcount function in the sys library
import sys
print('X is references:\t',sys.getrefcount(X), 'times')
print('Many of that is due to internal Python code')


