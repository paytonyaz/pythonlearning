# -*- coding: utf-8 -*-
"""
Created on Sun Jul 19 19:12:55 2020

@author: Bill
"""
# =============================================================================
# Different bases 
# =============================================================================

#python supports different bases
hex_val = 0xA
oct_val = 0o12
bin_val = 0b1010

# given a base 10 value, it can be converted to a hex val via the hex function
ten_in_hex = hex(10)
print('10 in hex is: ', ten_in_hex)
#but wait, what if we want this capitalized
print('10 in hex is: ', ten_in_hex.upper()) #much better
#  can also do this for binary
print('10 in binary is: ', bin(10))
# If one wants to get fancy, they can zero pad it with the "format" otpion
print('10 in binary, with zero padding is: ', format(10, '#010b'))
# the 0b can be dropped
print('10 in binary, with zero padding is, without the "b": ', format(10, '08b')) # note: the width was changed from 10 to 8, this is bceaseu the 10 from above took into account the "0b""

#to go back from a string to a number, use the int() function
print('to go back from a string to a number, use the int() function')
print('0xF to decimal is:', int('0xF',16))
print('Note that the base must be specified')

print('You can also inspec the number of bits needed to represent a value')
x = 256;
print('x = 256, this requires', x.bit_length(), 'bits to represent')

"""
Bit Operations
"""
# Bit shifting works well, unlike in matlab!
print('This is 10 in binary', bin(10))
print('This is the same value, bit shifted right by 1:',bin(10 >> 1))

# ANDing and ORing
print('Python can do bit level AND and OR operations')
first_val = 10;
second_val = 2;
print('The first value is: ', bin(first_val), 'The second is:', bin(second_val))
print('ANDing these two we get', first_val & second_val)
print('Like wise, we can OR values\nLet''s OR 10 and 5')
print('10 | 5 results in', 10 | 5)
print('Bitwise NOT uses the ~ symbol')
print('NOTting, 5 results in:', ~5)
print('Weird, right?\nThis is because Python is representing 5 as a signed value, internally')
print('For signed representation, when inverting each bit for a value of X, we end up with -(X+1)')


"""
Logical OR and AND
"""
print('logical OR and AND can be done too')
aa = 1;
bb = 0;
print('Variable aa = ', aa, 'variable bb = ', bb)
print('The AND of these is:', aa and bb, 'the OR of them is:', aa  or bb)

"""
The Division (By Tom Clancy)
"""
print('There are 3 types of division:')
print('\t"Classic", which uses the "/" sign')
print('"\tFloor", which uses the "//" sign')
print('\tAnd "Modulus", which uses the "%" sign')
print('10 / 4 is:', 10/4)
print('10 // 4 is:', 10 //4 )
print('10 % 4 is: ', 10 % 4)
print('The 10 % 4 returns a "2", because "2" is the remainder of the division operation')

# a closer look at the floor operation
print('19 // 10 = ', 19 //10)
print('11 // 10 = ', 11 //10)
print('-19 // 10 = ' , -19 //10)
print('-11 // 10 = ' , -11 //10)
print('Note: that the floor always moves the value down (makes sense)')

# if you really want to truncate, you can use the truc method
import math
print('Use the "trunc" method to perform true truncation')
print('floor of 2.5 =\t', math.floor(2.5))
print('floor of -2.5 =\t', math.floor(-2.5))
print('trunc of 2.5 = \t', math.trunc(2.5))
print('trunc of -2.5 =\t', math.trunc(-2.5))

# =============================================================================
# More Math stuff
# =============================================================================
# Some of the following code requires the import math line from above
print('The Math library has many function and vairables')
print('Pi is', math.pi, 'and e is', math.e)
print('sin(2*pi/180) = ', math.sin(2 * math.pi / 180))
math.sqrt(144)

# there are also built in functions
pow(2,4) # raises 2 to the power of 4
2 ** 4 # does the same thing
abs(-42)
min(-3, 4, 7, -9)
max(-1, 5, 7, 100)
sum([1,2,3,4]) #  note that the sum function must process a sequency, not individual arguments

"""
Random Numbers
"""
import random
# random number between 0-1
rnd_num = random.random()
#you can spec a random integer between a range
rnd_int = random.randint(1,10)
print('here is a random integer between 1 and 10:\t',rnd_int)

print('The random library, also allows us to randomly pick an item of a sequence')
monty_strings = ['Life of Brian', 'Holy Grail', 'Meaning of Life'];
print(' We have a list of 3 different strings \t', monty_strings);
print('Picking randomly, we get:\t', random.choice(monty_strings))
print('it can also, reshuffle a list')
suits = ['spades', 'hearts', 'diamonds', 'clubs']
print('We have a string of suits:\t', suits)
print('We can shuffle it:')
random.shuffle(suits)
print('We now have:\t', suits)


# =============================================================================
# Fractions
# =============================================================================
from fractions import Fraction
x = Fraction(1,3)  #this is 1/3rd
y = Fraction(4,6)

print('x and y are', x,'and', y)
print('math can be performed on these, and retain precision')
print('x+y is', x+y)
# these can be used with floating point vlaues
Fraction(0.25)

# =============================================================================
# SETs
# =============================================================================

#sets are unordered collections of unique and immutable objects
print('sets can be created several ways')
s_a = set([1,2,3,])
s_b = {3,4,5,6}

print('Set a:', s_a)
print('Set b:', s_b)

# set operations
s_a - s_b   # difference
s_a | s_b   #union
s_a & s_b   #intersection
s_a ^ s_b   #what do they have different

#thess can be done using methods too
s_a.difference(s_b)
s_a.union(s_b)
s_a.intersection(s_b) #tests what is similiar
s_a.symmetric_difference(s_b) #what is different

# things can be added too
s_a.add('a')
# and removed
s_b.remove(6)


print('items can be checked to see if they are in a set',
      '\nfor instance, in "a" in set s_a?\n','a' in s_a)

# sets can be created by looping through lists of stings
square_set = { x**2 for x in [1,2,3,4]}

# sets are handy for fitler repeat data
L = [1,2,3,1,4,5,6,7]
set(L)
# we can then conver this back to a list
L = list(set(L))
#keep in mind the order may change though, since sets are unordered

# set are useful for perfming equality checks too
L = [1,2,3,4]
LL = [4,3,2,1]
print('L =\t',L,'\nLL=\t',LL)
print('Are these two lists equal?')
print(L==LL)
print('But using sets, we can do a better check')
print('Are the contents the same?')
print(set(L) == set(LL))
print('the same can be done by sorting the lists')
print('sorted L is =\t', sorted(L),'\nsorted LL is =\t', sorted(LL))
print('are these equal?\n',sorted(L) == sorted(LL))

"""
Another example of sets
"""
engineers = {'Dominic', 'Jacob', 'Bill','Joe'}
managers = {'Travis','Joe'}

'Bill' in engineers  #is Bill an engineer

engineers & managers #who is both an engineer and mananger

engineers | managers  #everyone

engineers - managers #engineers that aren't managers

managers - engineers  #managers who aren't engineers

engineers > managers  #are all managers engineers (superset)
#said another way, is managers a subsetof engineers, no.

{'Bill', 'Dominic'} < engineers #are Bill and Dominic a subset of engineers

managers ^ engineers #who deoesn't belong to both



