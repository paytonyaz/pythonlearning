# -*- coding: utf-8 -*-
"""
Created on Mon Oct  5 19:45:20 2020

@author: Bill
"""
# =============================================================================
# While loops 
# =============================================================================
#simple while loop. Loops until x is empty
x = 'spam'
while x:
    print(x,end=' ')
    x = x[1:]  #slices off the first char

"""
Breaks, continues, pass and the 'loop ese'
"""
# Breaks:
    # these jump out of the closest enclosing lopp 

#continue:
    # jumps to the top of the closest enclosing loop
        # think of this as shortcircuiting back the top

# pass:
    # does nothing at all, it is an empty statment placeholder
    
# loop/else block:
    # Runs if and only if the loop is exited normally (i.e. without a break)
    

#example using continue
x = 10
while x:
    x -= 1
    if x%2 != 0: continue  #skips all odd numbers
    print(x,end=' ')  #this will print all even numbers
    
# there is a better way of doing this, without continue which should be used sparingly
x = 10
while x:
    x -= 1
    if x%2 == 0: 
        print(x,end=' ' )

# The loop/else
# the while loop has an optional else statement that occurs after it, this is a
# a part of the loops block, and thus is skipped if the "break" command is used
# this can be handy if once the loop ends, you want something to execute
# and exampl of this is when finding a prime number
num = input('Enter a numer to test (must be greater than 1):\t')
#catches an error
try:
    num = int(num)
except:
    print('Bad input')
else:
    x = num // 2 #some num greater than 1. Also this is a good starting point.
    #   2 is the smallest divisor, so we want to start at that point
    while x > 1:
        if num % x == 0: # it is an even divisor and thus not prime
            print(num, 'has factor', x)
            break
        x -=1  #moves down by one
    else:
        print(num, 'is prime')    # this is the 'else' for the while loop. it executs when the while exits normally
        

# =============================================================================
#For Loops
# =============================================================================

# a for loop creates a targer for any iterable (a sequence) and then steps 
# through this target. Essentially, it steps through whatever the object is.

# example
# this steps through the list, printing one element at a time
for target in ['spam', 'eggs', 'ham']:
    print(target)
    
#example summing a list
total_sum = 0
for x in [1,2,3,4,5,6]:
    total_sum = total_sum+x
print(total_sum)

# note: python has a built in function for this, called sum
print(sum([1,2,3,4,5,6])) #but it is fun making a loop

# for loops work for any sequence, such as strings and tuples
S = 'lumberjack' # a string
T = ('and', 'I am', 'ok') # a tuple

#Loop with a string
for x in S:
    print(x, end=' ')

# loop with a tuple
for x in T:
    print(x)

""" More on Tuples and for loops
"""
#when going through a sequence of tuples, the loop target can be a tuple as well
T = [(1,2), (3,4), (5,6)] # a list of tuples
# now we loop through the list, but with the targert as a tuple
for (a,b) in T:
    print(a,b) #this will print each set of values within the list

# the extended sequence assignments, covered in Chapter 11 can be used in for 
# loops too
a, *b, c = (1,2,3,4) #this is an extended sequence assignment
print(a,b,c)

# this now does the same thing in a for loop
# the *b collects all the middle terms. for each element of the list, one after
# another
for (a,*b,c) in [(1,2,3,4), (5,6,7,8)]:
    print(a,b,c)
    

""" 
Dicts and for loops
"""

# for dictionaries, it loops through the keys
D = dict(name='Bill', job='Engineer', height=6)
for x in D:
    print(x)

# if we want to print the values we can do this:
for x in D:
    print(D[x])

# using what we showed about the tuple and for loop, this can be done better
print(D.items())
print('D.items() displays teh key.value pairs of a dict')
print('THis can be used to iterate through')
# this will create tuples for both the key and value of the dict, then print it out
for (key,val) in D.items():    
    print(key, '=>', val)

"""
Matching items within a list
"""
keys = [(4,5), 3.14]
items = ['aaa', 111, (4,5), 2.01]
#this will search through the items and see if it contains any keys
for key in keys:
    if key in items:
        print(key, 'was found')
    else:
        print(key, "Wasn't found")
        
# note this could be run with two loops, one inner and one out, but using the 
# "in" function is cleaner.

# the following loop compares the contents of two sequences, and stores the common
# elements.
seq1 = 'spam'
seq2 = 'scam'
res = [] #empty list, to contain the common elements

for x in seq1:
    if x in seq2: #compares element of first seq to the 2nd seq contents.
        res.append(x) #add to the res list
print(res)

# the whole loop above could be coded into oneline, using list comprehension
[x for x in seq1 if x in seq2]

"""
Reading from a file
"""

#first generate a file
fname = open('demo_text.txt','w')
fname.write('First line\n')
fname.write('Second line\n')
fname.write('Third line\n')
fname.close()

#now the file will be read out one line at a time
# this is the best way to read text data, as it only loads one line at a time
# thus if the file is large, memory constraints are not a problem.
for line in open('demo_text.txt'):
    print(line.rstrip()) #print already has a \n, so removes the one from the file

    
"""
Using the Range function
"""
# The range function generates a iterable, which may be used for indexes within
# a for loop
# To display it properly, it must be wrapped in list(), when sending to print()

#this creates a range from 1 to 4
print(list(range(1,5)))

# this is 2 to 4
print(list(range(2,5)))

# this will count by 2 from 0 to 9
print(list(range(0,10,2)))

# the step size is given by the third argument. It can be negative too
print(list(range(5,-5,-1)))

# range comes in handy, to specify the number of times you want to do a loop
#this loop executes 3 times (not how when not specifying the start of a range, 0 is assumed
for i in range(3):
    print(i, 'pythons')
    
# for simple loops, it is generally better to allow python handling the index,
# and not use range() directly. However, using range does allow more flexible index
# as shown below
S = 'spam'
print(S)
for i in range(len(S)):
    S = S[1:] + S[0]  #moves the first element to the end
    print(S, end=' ')
    
"""
Parallel Traversals with Zip and Map
"""
# zip can be used to create a combination of tuples, which allows two sequencies 
# to be iterated through

L1 = [1,2,3,4]
L2 = ['one','two','three','four']
# say one would like to sequence through both of these. 
# this can be done by zipping them up
z1 = zip(L1,L2)
# when zipped, this creates a paired tupple. [(1, 'one'), (2, 'two')...] and so on
# like range() the zip objects must be converted to a list for printing
print(list(z1))

# loops through both lists in parallel
for (x,y) in zip(L1, L2):
    print(x,'=', y)
    
# zip can be used on more than 2 object
T1, T2, T3 = (1,2,3), (4,5,6), (7,8,9) # defines 3 tuples   
print(list(zip(T1,T2,T3)))    # this pairs the elements of the 3 tuples together
# you will get [(1,4,7), (2,5,8)...]

# if using zip on uneven length objects, it ignores the extra elements
# for instance
S1 = 'abc'
S2 = 'xyz123'
# this will ignore the remaining three elements of S2.
print(list(zip(S1,S2))) 

#Map allows you to link function outputs to an iterable
# for instance, the ord() function returns the ASCII value of a char.
print(ord('A'))
# if we want to do this for a whole sequence, we can do this:
char_out = map(ord, 'spam')
print(list(char_out)) #this prints out the values of all the char of "spam". Like range, this must be wrapped in list()

#the above funcitonality could be done with a loop too:
result = [] 
for char in 'spam':
    result.append(ord(char))
print(result)
# map will be covered in more detail in Chapter 19 and 20.

# zip can be used to construct dictionaries from two collections of lists
# this is handy if the key and values are created during run-time, not predefined
# for isntance, say the following key value pairs were gathered from the user.
keys = ['spam', 'eggs', 'toast']
vals = [1,3,5]

# first way of using zip
D2 ={}
for (k,v) in zip(keys,vals):
    D2[k] = v
print(D2)

# Second way, which is quicker and doesn't need a for loop
# the dict item automatically constructs a dictionary from the tuple pair created by zip.
D3 =dict(zip(keys,vals))
print(D3)

# Using enumerate to keep track of an index
# it is often usefull to have the index and the item at that index when going through 
# a loop. This was one reason to use the range function, a shown aboce.
# another way of doing this is to use the enumerate method, which gives a 
# index for free. 

#example using a for loop while declaring an index variable
index = 0
S = 'spam'
for char in S:
    print('The char "', char, '" occurs as the', index,' item')
    index += 1

# example using enumerate
for (index, char) in enumerate(S):
    print('The Char "', char, '"occurs as the', index, 'item')
# in this example, enumerate provides a free index value.
# enumerate will be covered more in future chapters.

# =============================================================================
#      Looping system info Data
# =============================================================================
# the popen method from the os module can be used to grab system info:
# infact it can be used to read anything that a command prompt will return
import os
F = os.popen('dir')
F.readline() #reads the first line of the return value

# we can loop through this:
for line in os.popen('dir'):
    print(line.rstrip())

# this may be useful to determine the type of system the code is running on
for line in os.popen('systeminfo'):
    print(line.strip())

# that returns a lot of data, so let's try to clean it up a bit. This prints the
# first 4 lines, and then breaks
for (index, line) in enumerate(os.popen('systeminfo')):
    if index == 4:
        break
    print('{0:0>5d}) {1}'.format(index,line.rstrip()))
    
# This one prints only the 'system type' information
for line in os.popen('systeminfo'):
    part = line.split(':') # delimintes on the ":"
    if part and part[0].lower() == 'system type':
        print(part[1].strip()) #this strip() removed all the empty white space
        break
    

        
        
        