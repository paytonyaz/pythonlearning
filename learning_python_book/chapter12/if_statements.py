# -*- coding: utf-8 -*-
"""
Created on Sun Oct  4 17:56:39 2020

@author: Bill
"""
#simple if statement

x = 'killer rabbit'
if x == 'roger':
    print('shave and a haircut')
elif x == 'Bugs':
    print("What's up Doc?")
else: 
    print('Run away!')

# Python does not have a switch or case statement, unlike C or Matlab. 
# Many times, dictionaries can be used in their stead.

"""
Example of using a dict as a switch statement:
"""
#sets up a dictionary
meat_price = dict(spam=1.25, ham = 1.99, turkey = 1.75, chicken = 1.10) 
#asks for an input
choice = input('Enter a lunchmeat:\t')
# this prints the output, based on the input. This is very similar to how a 
# switch statement would work, but was implemented via a dictionary
print(meat_price.get(choice.lower(),'Not an option'))

"""
Ternary Statements
"""
#This is the "expanded" version of the ternary operation
X = False
if X:
    print('True')
else: 
    print('False')
# because C has a way of doing this in one line, a way of doing this was added to Python

#Below is the one line equivalent
print('True') if X else print('False')

"""
Booleans
"""
#Booleans operate similar to other languages, however the return of an AND or an OR
# are in fact the values, not just a 1 or a 0.
# for example

# X = A or B or C or None, this assigns the first non-empty value to X.
#an example that can be run
X = [] or 5 or 1  #assigns 5 to X, since it is the first non-zero value
print('The outcome of: "4 or 5" is:')
print(4 or 5)  #this prints out 4. (Note, in Matlab this is a '1', since it is true)

#and is the opposite. Any non-zero value will make the result zero. This means that
# that in a set of all non-zero values, the last value is assigned is returned
print('The output of: "4 and 5" is:')
print(4 and 5)  #this prints out 5, as both sides need to be looked at to determine the AND





