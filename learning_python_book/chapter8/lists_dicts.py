# -*- coding: utf-8 -*-
"""
Created on Fri Sep  4 07:15:52 2020

@author: wwilkinson
"""


# =============================================================================
#  Lists:
# =============================================================================


"""
Basic Operation
"""
#lenth can be determined:
print('The length of [1,2,3] is', len([1,2,3]))

# concatenation
aa =[1,2,3]
bb = [4,5,6]
cc = aa+bb

#the "=" operator must be paired with another list
# this breaks: aa + '34'
# but this works
aa + ['34']

#repitition
['Ni']*4


# Slicing
L = ['spam', 'Spam', 'SPAM']
print(L)
L[1] = 'eggs'; # replaces the middle term
print(L)
L[0:2] = ['eat', 'more'] # replaces the first two items 
#(remember the last term in the slice operation is non inclusive)
print(L)
#note the sizes don't need to match
L = [1,2,3]
print(L)
print(L[1:2])  #L[1:2] is only the 2nd element
L[1:2] = [4,5] # replaces the single elment value with two.
print(L)
# you can also use this feature to just replace, not remove
L[1:1] = [5,6]  #this inserts two elements, starting at index 1. Nothing is removed
print(L)

# one can use the same strategy to delete elements
L[1:2] = []  # this removes the elemnt at index 1
print(L)
#note, this doesn't work when specifyin the exact element. Must be done with a slice assignment
L[1] = [];  #this actually replaces the element at offset of 1 with an empty list. Weird.

"""
Method Calls
"""
# Note that since lists are mutable, the method calls actually alter the item you call
# it on. So L.append, actually appends to L.

L = ['eat', 'more', 'SPAM!'];
L.append('please')  #using a method to extend the list 
L.sort() #sorts the array. Note that 'S' is comes before 'e'. 
print(L)

L = ['eat', 'more', 'SPAM!'];
L.append('please')  #using a method to extend the list 
# inserting elements
# the "insert" method can be used. Howeve, note how this actually imbeds another list
# into the main list, L
L.insert(3,['and','eggs'])
print(L)
# there is another way of doing this

L = ['eat', 'more', 'SPAM!', 'please'];
print(L)
#first find the offset you want to use
offset = L.index('please')
# then use this index as both arguments in a slice operation.
L[offset:offset] = ['and', 'eggs']
print(L)

# More on sorting
L = ['abc', 'ABD', 'aBe'];
print(L)
L.sort()
print(L) #note how the case has corrupted the sorting
L.sort(key=str.lower) # this normalizes to lower case
#you can also change order
L.sort(key=str.lower, reverse=True)
# there is also a sorted built in function (instead of a method)
L2 = sorted(L,key=str.lower)
print(L2)


# there is also an "extend" method, which is similar to the append method
L = [1,2,3];
L.extend([4,5,6])

L2 = [1,2,3]
L2.append([4,5,6])
#note that the "append" method imbeds another list withinthe list (similiar to the insert above)
# the "extend" method adds the elements directly.

#pop, returns the last value and then removes it
print(L.pop())

# deleting elements
#individual elements can be delted with the "remove" method
L = ['eggs', 'bacon', 'ham']
print(L) 
L.remove('bacon')
print(L)

# lists can be reversed
#L.reverse()
#and again
L.reverse()
#this also works with heterogeneous lists
L = [1,2,'one','two']
L.reverse()

# =============================================================================
# Dictionaries
# =============================================================================

D = {} #emptu dict
D = {'name': 'Bob', 'age': 40} #two item dict
E = {'cto': {'name': 'Bob', 'age': 40}}  #nested dict
#alternative construction
D = dict(name='Bob', age=40)
#Note: For this alternative implementation, the keys must be "well behaved" 
# strings. When using curly braces, the keys can really be anything. So that
# way provides more freedom.

print('The name is', D['name'])
E['cto']['name'] #indexing nested dict

""" Methods
"""

print('The keys present in D are:')
print(D.keys())
#these can be placed in a list
list(D.keys())
print('The values withn D are:')
print(D.values())

#all contents can be pulled:
D.items()

#dict can be copied
D2 = D.copy()

#remove all items
D2.clear() #this leaves it as an empty dict

#dictionaries can be merged by key
D = dict(name='Bob', age=40, car='Audi')
print('Items in D:')
print(D.items())
D2 = dict(name='Bill', age=35, height=6)
print('Items in D2:')
print(D2.items())
D.update(D2) # note that all the fields from D2 were brought into D. But the unique field in D maintined.
print('New Items in D:')
print(D.items())

# the method 'get' can be used to specify a return value if a key is not present
# D['hair'] this errors out
D.get('hair',-1) # this returns a -1, since hair is not a valid key.

# pop out a key
car_type = D.pop('car',-1) #this returns a value, and removes it from the dict. 
#2nd argument is the return value if key doesn't exist


"""
Functions
"""
#the len function works
len(D)
# also the "del" keyword works, to remove an item
list_o_keys = list(D.keys())
del D[list_o_keys[0]]  # deletes the first key, which is stored in the list lost_o_keys

"""
Movie Database Example
"""


movie_table = {'1975': 'Holy Grail', '1979': 'Life of Brian', '1983': 'The Meaning of Life'}
year = '1983'
movie = movie_table[year]

for year in movie_table:
    print(year + '\t' + movie_table[year])

"""
New way of initilizing
"""

# using "from keys""
# this provides a way to initilize a dict with a default value associated with all keys.
D = dict.fromkeys(['a', 'b'], 0)

# the zip fuction can be used to combine two lists into a key/value pair
# which then can be used to create a dict.
D = dict(zip(['a','b','c'], [1,2,3]))
# this is handy for dynamically making dictionaries.

# to see what zip does:
print(list(zip(['a','b','c'],[1,2,3])))
# note: to print it, it must be turned into a list. Otherwise it is just an 
# iterable, which isn't usefully printed.
# this creates a list of tuple pairs. These pairs represent the kyey/value pair in the dict

# using comprehension:
# this loops through a list, and creates a dict.
D = {x: x**2 for x in [1,2,3,4]}
print(D)

# =============================================================================
# Quiz
# =============================================================================

# 1. Show two ways to build a list of 5 elements, all 0

list_1 = [0,0,0,0,0];
list_2 = [x*0 for x in range(1,6)]
list_3 = [0]*5   # this is the "correct" answer

#2. Show two ways to build a dict with two keys (a,b) both with value 0
D1 = dict.fromkeys(['a','b'],0)
D2 = {'a': 0, 'b':0}





