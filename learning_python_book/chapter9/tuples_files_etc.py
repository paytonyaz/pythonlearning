# -*- coding: utf-8 -*-
"""
Created on Sat Sep 12 09:41:41 2020

@author: Bill
"""
# =============================================================================
# Tuples
# =============================================================================
# tuples are ordered collections of immutable elements. 

# Initilizing
T = (); #an emptu tuple
T = (0,) # note the "," must be used at the end, otherwise python will simplify this to an int
# this is not needed when making a multielement tuple.
T = (0, 'ni', 1.2, 3)  # a 4 element tuple
T = 0, 'Ni', 1.2, 3; # no actual need for parathensis
T = tuple('spam'); #turns the string into tuple.

""" Functions and Methods
"""
T = (1,2,3)
T2 = (4,5,'six')

print('Tuple 1 is', T, 'Tuple 2 is', T2)
print('Tuples cen be combined, T1+ T2 is:', T+T2)

print('Repitition also works')
T3 = ('Ni',)*3

#they are accessed by offset
print('The 2nd element of T is:',T[1])
# the function len also works
len(T)
# the sorted function works too
T_reverse = sorted(T,reverse=True)

# Methods
print('There are fewer methods for tuples than lists')
T.count(1)  # counts the number of occurances
T.index(1) # returns the index corresponding to the argument
T = (1,2,3)*3
T.index(2,2) #index of the second occurance of 2
print('How maany "2s" are there in T?')
print(T)
print(T.count(2))

# Nested Tuples
# this contains a list within a tuple
T = (1,2, [1,2], 3, 4)
# T[0] = 5, will fail. because tuples are inmutable
T[2][1] = 3; #this works, since the list within the tuple is mutable 

# =============================================================================
# Named Tuples
# =============================================================================
# named tuples can be imported from the collectons module
# this is kind of a mix between tuples and dictionaries.
from collections import namedtuple
# this creates a generated class
Rec = namedtuple('Rec', ['name', 'age', 'jobs'])
# this then makes a named tuple.
bob = Rec(name='Bob',age=40.5, jobs=['dev','mgr'])
#this can then be accessed by position or referece.
bob[0]
print(bob.name)
print(bob.jobs)


# =============================================================================
#  Files
# =============================================================================

# writing to a file
fname = 'test_file.txt' #this can contain a file path too. Without it, assumes in the same location as the script
f_object = open(fname,'w') # opens a file for writing
f_object.write('This is a string I am writing to a file')

# reading file
f_read = open(fname,'r')  # the 'r' means read. This is the default behavior, does not need to be included
astring = f_read.read(); #this pulls in the string from the file
print(astring)
f_read.close()

# writing multiple lines
fname2 = 'test_file2.txt';
f_write = open(fname2,'w')
f_write.write('This is the first line\n')
num_chars = f_write.write('Now I am writing the 2nd line\n') # a write function returns the number of chars written
f_write.close()

# reads one line at a time
f_read = open(fname2)
print(f_read.readline())  #reads the first line
print(f_read.readline())  #reads the 2nd line
print(f_read.readline())  #this will return an empty string
f_read.close()

# reads the whole contents
print(open(fname2).read())

# using iterators to read the contents of a file
for line in open(fname2):
    print(line, end='') # the end='' ensures there is no space between multiple lines (i.e. not double spaced)


# writing different data types to files
X,Y,Z = 43,44,45; #declares some integers
S = 'Spam';
D = {'a':1, 'b': 2} # a dict
L = [1,2,3]  #a list

F = open('datafile.txt','w')
F.write(S+'\n') #adds a newline after the string
F.write('%s,%s,%s\n' %(X,Y,Z))  #converts the ints to strs and then prints them to file
F.write(str(L) + '$' + str(D) + '\n') #converts list and dict to string, separates with $
F.close(); 

# reading the contents back
chars = open('datafile.txt','r').read();
print(chars)

# now lets try and convert the contents back to objects
F = open('datafile.txt','r')
line = F.readline()
print(line) # this gives us the first line, 'Spam'
#we then need to remove the \n char
newS = line.rstrip()

# now for the second line
line = F.readline()
# this pulls in a a string of '43,44,45'
print(line)
#we want to split this
parts = line.rstrip().split(','); #this removes the \n then splits, using commas as delimintors
print(parts)
# now we just need to convert the strings back to ints
numbers = [int(P) for P in parts] # this is now a list of the original ints

# 3rd line time
line = F.readline();
print(line) # this is the line with the list and dict, seperated by '$'
parts = line.split('$') #split along the '$'
print(parts) #this creates a list
# not lets isolate the left half, which we want to become a list of numbers
subpart = parts[0]; #this pulls out a list, of strings
#this removes the [ and ] and also splits on commas
no_parenthesis = subpart.replace('[','').replace(']','').split(',') #this is now a list
numbers = [int(num) for num in no_parenthesis]

#however there is an easier way to handle all of this. That is to use the eval
# command. This treates a passsed string as a python command. Since the string
# represent the objects we want to recreate, it is as simple as passing them 
# to eval
list_o_nums = eval(parts[0])
dict_of_terms = eval(parts[1])

# =============================================================================
# Pickle
# =============================================================================
# Pickle is a way to store python objects directly, instead of saving them
# as strings in a text file. 
# Think ofit as a .mat equivilent, but in python.

D = {'a': 1, 'b': 2}
F = open('datafile.pkl','wb')
import pickle
pickle.dump(D,F);
F.close();

# to get the dictonary back
F = open('datafile.pkl','rb') #the 'rb' means read a binary file
E = pickle.load(F)
print(E)

# =============================================================================
# JSON
# =============================================================================
# JSON is a language independent storage format. The method of storing aligns
# well with python. It is very similar to a multilayerd list or dict.

#create a nested dict to be saved in JSON format
name = dict(first = 'Bob', last = 'Smith')
rec = dict(name=name, job = ['dev', 'mgr'], age = 40.5)  #this takes in the named dict, as a term.
print(name)
print(rec)

# now saves as a Json format
import json
# the following couple of lines use the "dumps" method. This allows the results
# to be printed to strings, to be inspected. Pickle has the same method
print(json.dumps(rec))
S = json.dumps(rec);
# now reads it back
L = json.loads(S);

#now saves the json to a file
fp = open('testJSON.txt','w')
json.dump(rec,fp,indent=4)
fp.close()
#prints what was saved to file
print(open('testJSON.txt','r').read())

#loads the json data
P = json.load(open('testJSON.txt')) #reads the data in. It is now a dict again.
print(P)

# =============================================================================
# CSV files
# =============================================================================
# Python has a built in csv module too. The book mentions this doesn't interface
# as niceley as the pickle or json modules though
import csv
aa = list(range(1,21))
f_csv = open('csvtest.csv','w')
csv_writer = csv.writer(f_csv)
csv_writer.writerow(aa)
f_csv.close()

# From talking with Andrew. Pandas may be a better way of reading/wrting to 
# csv files


# =============================================================================
# Chapter Quiz
# =============================================================================

#1 How to determine the size of a tupple?
T  = (1, 2, 3)
print('The Tupple T:', T)
print('This tuple is ', len(T), ' characters long')

# 2. Change the item in tuple (4,5,6) to (1, 5, 6)
T = 4,5,6; #original tuple
#because tuples are immutable. We can't directly reassign this value. Instead
# we need to reassign all of T
T = (1,) + T[1:] #creates a new tuple with the desired value

# 5 How do you copy all parts of a nested structure, all at once?
# use the deepcopy method from the copy  module
# this is rarely done



