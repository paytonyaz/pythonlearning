# -*- coding: utf-8 -*-
"""
Created on Fri Oct  2 07:54:26 2020

@author: wwilkinson
"""

# getting user input with a while loop

while True:
    reply = input('Enter Text:\t')
    if (reply.lower() == 'stop'): break  #this ends the while loop. 
    # Note, by calling .lower() this allows "Stop" or sTop" to end the loop
    print(reply.upper()) #prints out the capitlized version of the text
    
# similiar thing, but does squaring
while True:
    reply = input('Enter a number to Square:\t')
    if not(reply.isdigit()):
        if reply.lower() == 'stop':
            break  #exits the loop
        print('Please enter a number')
    else:
        print(int(reply)**2)  #squares the number
        
# does the same thing, but uses "try" for error handling
while True:
    reply = input('Enter a number to square:\t')
    if (reply.lower() == 'stop'): break 
    try:
        num = int(reply)
    except:
        print('Bad input')
    else:  #this is associated with the try. It goes try, except, then else
        print(num**2) 
print('Bye')  #when stop is entered

    
# silly nested loop which counts to 100,000    
count = 1
while True:
    count += 1
    if (count % 100) == 0:
        print(count, '\tDivisible by 100')
    elif (count % 1313) == 0:
        print(count, '\tDivisibe by 1313')
    if count > 100000:
        break
print('bye')



    
