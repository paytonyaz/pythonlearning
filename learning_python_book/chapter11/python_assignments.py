# -*- coding: utf-8 -*-
"""
Created on Sun Oct  4 11:24:25 2020

@author: Bill
"""
# =============================================================================
# Assignments
# =============================================================================
# the most basic is the quals sign

Var1 = 'spam'

# tupple uppacking
tup1, tup2 = 'yum', 'YUM' # this will create two tuples with the values to the right

#list unpacking
[string1, string2, num1] = ['yum','YUM', 5]  #this creates 3 variables and assings them a value based on position

# sequence assignments 
a,b,c,d = 'spam'  #a = 's', b='p' and so on

# Extended squence unpacking
# a = 's' and the wildcard assigns the rest of the string to b (in this case 'pam')
a, *b = 'spam'

#mulitple target assignments
var1 = var2 = 'lunch' #assigns 'lunch' to both variables.

#augmented assignments
spam = 0
spam += 42  #is the same as saying: spam = spam+42

# Using tuple assignmnets to swap values
a = 'letter A'
b = 'letter B'

(a,b) = (b,a)  #swaps the values


# assigning using the range function
red, blue, green = range(3) #red = 0, blue = 1, green = 2

""" More on extended sequencing unpacking
"""
seq = list(range(1,6))  # creates a list 1-5
#normal unpacking
a,b,c,d,e  =seq  #assigns one value to each variable

# the remaining right most portion goes to b
a, *b = seq  #a gets 1, b gets the remainder

# collects the first portion
*b, a = seq  #now b gets the first part, a gets just the ending value

#middle part
a, *b, c = seq # a and c get the first and last. b gets the rest

# the same concept works with any iterable.
#a and b get the first two values. c collects the rest. Note, it is returned as a list though, not a string
a, b, *c = 'this is a string' 


