# -*- coding: utf-8 -*-
"""
Created on Sun Oct  4 12:38:50 2020

@author: Bill
"""
# =============================================================================
# Playing with the print statement
# =============================================================================

x = 'spam'
y = 99
z = ['eggs']
print(x,y,z) #default printing

#by default there is a space between each object which is sent to the print statement
# this can be changed
print(x,y,z,sep='') #changes the sep to nothing

#or it can be changed to something else
print(x,y,z,sep=', ') #changes the sep to a space and comma

#similiarly, the end of the print statement can be changed from the default of \n
print(x,y,z,end='...\n') #adds in three dots

"""
Redirection to a file
"""
# this will print to file, instead of the prompt
print(x,y,z,sep='...', file=open('data.txt','w'))
#now loads back in and prints contents
print(open('data.txt').read())

# this can be done more generally by changing the "standard" output stream in python
import sys
original_output = sys.stdout  #this saves the standard output stream
sys.stdout = open('log.txt','a') # sets output to a log file.
#now all the following print statments will go to the log file
print('spam')
print('This is now going to a log file', [1,2,3], 'Oh boy!')
sys.stdout.close()  #closes the log file. This ensures the contents are flushed
sys.stdout = original_output  #restores the behavior

# reads the contents of the log file
print(open('log.txt').read())

# =============================================================================
# Quiz
# =============================================================================

# 1: Name 3 ways you can assign the same value to three variables

a = 1
b = 1

a = b = 2  #assigns 2 to both a and b

a,b = [3] *2  #creates a list, with values of 3, then assigns each element to a, or b
