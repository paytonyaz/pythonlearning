# -*- coding: utf-8 -*-
"""
Created on Thu Aug 20 07:32:56 2020

@author: wwilkinson
"""

# =============================================================================
# Strings!
# =============================================================================

S = ''; #Empty string
S = "Spam's" # double quotes work too, and allow inclusion of single quites
print(S)
S = 'Spam''s' #Note, this doesn't work the same as above, instead you will get "Spams" (no single quote)
print(S)
S = 'Spam\'s' #however you can use the escape char to print the extra '
print(S)

S = 's\np\ta\x00m'; #special escape char: new line, tab, and extra spacing
print(S)

S0 = 'Spam'
S1= 'Spa\x00m'; #The addition of "\x00" increases the spacing between char.
print(S1)
# The reason \x00 increases the spacing, is "\x" is the escape char for Hex. 
# Then when passing to the print function python interprests this hex value and
#  replaces it with the Unicode (I think) representation of that Hex value.
#  The representation of \x00 (or code 0) happens to  be a blank space (Taylor Swift understands)
#  This can be done with other values. For instance:
S2 = '\x41\x42\x43\x01'
print(S2)

S = """...mulitline..."""
print(S)

S = 'spam'
S = r'\temp\spam' # this is a raw string, with no escape char

"""
Some other operations
"""
S = 'Spam'
# concatenation
print(S+S1)
#repeat
print(S*4)
#slicing
print(S[0])
#determining length
print('The string:',S,'Is',len(S),'Char long')

# string formatting
kind = 'blue'
print('a %s parrot' % kind)
print('a {0} parrot'.format(kind)) #another kind of formatting

"""
Various Methods
"""
S.find('pa')
S.rstrip() # removes white space

S.replace('pa','xx')
print(S)

S1 = 'This,or,that'
S1.split(',') # splits on deliminator
print(S)

S2 = '5'
print(S2)
print('is S2 a number?:', S2.isdigit()) #checks to see if it is a digit

#case conversion
print(S.lower(), S.capitalize(), S.upper())

#End cehck
print('Does', S,'End with \'m\'?')
print(S.endswith('m'))

# looping
for x in S:
    print(x)

"""
Slicing
"""
S = 'Spam'
#slicing works slightly differently than Matlab.
S[0] #grabs the first item
S[1:] #grabs 2nd through last.

S[:3] # this will grab from the beging to the 2nd value (the end point is always noinclusive)
# because no "starting offset" was provided, it defauled the begining of the string

#can also do negative offsets. S[1:]
S[1:-1] #will grab the 2nd value through to one minuse the end value
# think of the negative offset S[-1] as S[len(S)-1]
S[-1] # this grabs the last point

#there is also a way to define the "stride". This is where the difference from 
# Matlab really comes into play
# S[a:b:k] grabs values from index [a] to (but not including) index [b] and taking steps of [k]
# by default [k] is one
#lets see an example with a list
my_list = [1,2,3,4,5,6,7,8,9,10];
my_list[::2] #goes from start to finish, grabing every 2nd element
#you can also use (-) strides
my_list[::-1] #this counts backwards

"""
Converting
"""
S = '42'
int(S) # converts to an integer
S = '3.14'
float(S) #converts to a float

ord('a') #returns the value used to represent the char
chr(97) #does the opposite

# Replacing
S = 'xxxSPAMxxxSPAMxxx';
S.replace('SPAM','EGGS') # this is a global replace
S.replace('SPAM','EGGS',1) #this only replaces the first occurance

#note that the string was never replaced. S, remained the same. This is because strings
# are immutable. As a result a new string needs to be created.
# this can cause problems if doing lots of alterations on large strings
# one way around this is to conver a string to something that is mutable, like a list

L = list(S) # converts to a list
print(S)
print(L)
L[3] = 's' #replaces a specifc element of the list
S = ''.join(L) #coverts the list back to a string
print(S)

#note that join is interesting. It combines to objects with the desired deliminter (the main object string)
# in the first example, the deliminter was a blank string, so nothing
# in this example, the deliminter is another string
S = 'SPAM'
S2 = S.join(['eggs', 'ham', 'sausage', 'toast'])
print(S2) #here, string S, is used as the deliminter, seperating the different elements of the passed list to the join mehtod.

"""
Text parsing
"""
line = 'aaa bbb ccc'
cols = line.split(); #splits based on deliminter, default is white space
line = 'bob,hacker,40';
cols = line.split(',') #splits along commas
#delimintes can be a string
cols_s2 = S2.split(S) # uses 'SPAM' to split S2

line = 'The knights who say Ni!';
line.isalpha() #isalpha checks to see if there are only letters. So the "!" and white space is causing it to return Fals
S3 = line.replace('!','').replace(' ','')
S3.isalpha()

print('Where does "Ni!" start in string: ', line, '?')
print('It starts at line: ',line.find('Ni!'))
print('If the phrase didn\'t exist, a -1 would be returned')
S = 'Spam'
print('Is "Ni!" in string: ', S, '?')
print(S.find('Ni!'))

# =============================================================================
#  String Formatting
# =============================================================================

# there are two ways:
    # the older way, which uses expressions (think printf in C or sprintf in MATLAB)
    # the newer way, using the .format method and {}, similiar to C#
   

## Expression
S = 'That is %d %s bird' % (1, 'dead')
print(S)
# note the % indicates placeholders, and then seperates the string from the values, which are contained in tuple.

# note how the below example just used the %s for all three values
# this is because in Python eveything can be converted to a string,
# so the %s placeholder, many times, is all that you need
S = '%s -- %s -- %s' % (42, 3.14159, [1, 2, 3])
print(S)

# normal, left justification with 6 width, normal justification 6 width and 0 used as filler
x = 1234
res = 'intergers: ...%d...%-6d...%06d' % (x,x,x)
print(res)

x = 1.2345
res2 = '%f...%.2f...%5.2f...%-5.2f...%05.3f...%f' %(x,x,x,x,x,x)
print(res2)

# passing an option formatting value
# in the next line, the '*' indicates the formatting value is also passed in the tupple to the right
# in this cae the '3' this is handy if the reoltuion is not known beforehand. In this way it can be set via some equation or variable.
res3 = '%f, %.*f' %(x,3,x)
print(res3)


# Using Dictionaries
# the keys of a dict can be used, with the %, as placeholders
S = '%(qty)d more %(food)s' % {'qty':1, 'food':'spam'}
print(S)


## formating Method based

#values can be specified by:
    #position
template = '{0}, {1} and {2}'
print(template.format('spam','ham','eggs'))

    # keyword
template = '{motto}, {pork} and {food}'
print(template.format(motto='spam',pork='ham',food='eggs'))

    # both
template = '{motto}, {0}, and {food}'
print(template.format('ham',motto='spam',food='eggs'))

    # relative position
template = '{}, {}, and {}'
print(template.format('spam','ham','eggs'))

x = 3.1459
res = '{0}...{1:.2f}...{2:.3f}'.format(x,x,x)
print(res)

#this can be nicely shortened
res = '{0}...{0:.2f}...{0:.3f}'.format(x)
print(res)

# playing with alignment
#normal; default right alignment, 10 char val, 2 decimals; left alignment 10 char values, 2 decimals;
res2 = '...{0}...{0:10.2f}...{0:<10.2f}...{0}'.format(x)
print(res2)

#filling
#fills with 0. both for left and right alignment
res3 = '...{0}...{0:0>10.2f}...{0:0<10.2f}...'.format(x)
print(res3)


# it is useful for the thousands seperator
x = 999999
template = '{:,d}'
print(template.format(x))