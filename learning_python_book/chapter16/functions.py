# -*- coding: utf-8 -*-
"""
Simple function
Created on Sat Oct 24 19:44:05 2020

@author: wwilkinson
"""

def times(x,y):
    """
    Parameters
    ----------
    x : first arg
    y : second arg

    Returns
    -------
    product of x and y.
    """
    return x*y

print(times(22,3))
print(times('Ni',4))


def intersect(seq1, seq2):
    """
    Returns the common elements of two sequences

    Parameters
    ----------
    seq1 : TYPE
        DESCRIPTION.
    seq2 : TYPE
        DESCRIPTION.

    Returns
    -------
    common elements of two sequences

    """
    res = []
    for x in seq1:
        if x in seq2:
            res.append(x) #adds to x
    return res

s1 = 'Spam'
s2 = 'Scram'
common = intersect(s1, s2)
print(common)
# in reality this feature is easier to code as a single line
common = [x for x in s1 if x in s2]
print(common)


