# -*- coding: utf-8 -*-
"""
Created on Sun Oct 18 20:25:10 2020

@author: wwilkinson
"""

# =============================================================================
# Iterators
# =============================================================================
#An iterator or iterable is any object which can be iterated through.

# create a demo file
F = open('demo.txt','w')
F.write('First line\n')
F.write('Second line\n')
F.write('Third line\n')
F.close()

# as covered before, the best way to read lines from a file is within a for loop
# this is faster and more efficient, than using the readlines() method
for line in open('demo.txt'):
    print(line,end='')
    
# the file loading shown above is possible because files are iterable. this is done
# internally within python by calling the _next_ method.

#for instance,this can be done by:
F = open('demo.txt')
print(F.__next__()) #prints 'First Line'
print(F.__next__()) # prints "Second line"
print(F.__next__())    #prints "Third line"

#the same thing is done with a built in function called next.
#this calls the __next__ method, but is just a simpler and way of doing it
F = open('demo.txt')
print(next(F))
print(next(F))
print(next(F))

# This is how all iteration works in python. The __next__ method of an iterator
# object is called, moving from one element to another
# However, for some objects, the iterator must be created (note how for files...
# the object was already in iterator, in that the __next__ method was present)
# for lists, this iterator must be created. This is done by calling the iter()
# function, which then calls the __iter__ method, constructing the iterator,
# allowing the __next__ method to be called, and step through that object
# this all happens automatically when calling a for loop.

# showing the manual creation of an iterator with a list
L = [1,2,3] # a list
I = iter(L) #creates an iterator object, this now has the __next__ method
# now the lements can be stepped through
print(I.__next__())
print(I.__next__())
print(I.__next__())

# using this, we can see how the enumerate tool works
E = enumerate('spam') # creates a enumerate object
I = iter(E) #turns it into an interator
print(next(I)) #this returns both the index and the value
print(next(I))
# the full iteration can be forced by wrapping it in list
print(list(enumerate('spam')))

# =============================================================================
# List comprehension
# =============================================================================
#list comprehension is a compact way to form list
# say you wanted to add 10 to every element within a list.
# this could be done with a for loop like so
L = [1,2,3,4] #creates the original list
for ii in range(len(L)):
    L[ii] += 10 #add 10 to each element
print(L)

#with list comprehension this is easier and faster (operates at C speed)
L = [1,2,3,4] #creates the original list
L = [x+10 for x in L] # creates the new list with 10 to every element
print(L)

#list comprehension goes like so:
#[ arbitraryExpression, forLoopHeader, iterableObject]

""" 
List Comprension with Files
"""
# a couple examples with files
# one can read all the lines of the file using the readlines method
F = open('demo.txt')
lines = F.readlines() #dumps the whole contents into lines
print(lines)
# you will not that when reading the raw output of the file there is a "\n" after
# every item. This is annoying and can cause problems with printing.
# since we would want to fix this for every line, this a perfect thing to iterate
# we could do this with a for loop, but lets use list compr.
no_newLine = [line.rstrip() for line in lines] # this iterates it all away.
print(no_newLine)  
    
#however the whole operatation can be done better. For one, it is not good to dump
# the whole contents into memory, this breaks down with very large files.
# this was fixed before by using a for loop, to read one line at a time
# since it can be done in a for loop, it can also be done with list compr.
better_way = [line.rstrip() for line in open('demo.txt')]
print(better_way) # above was faster, and simpler.

#what is cool, is multiple string operatation can be strung together.
caps = [line.rstrip().upper() for line in open('demo.txt')]
print(caps)

#or this
seperate = [line.split() for line in open('demo.txt')]
print(seperate)

# or this
excited = [line.replace(' ', '! ').replace('\n','!') for line in open('demo.txt')]
print(excited)

# or this, which checks for the presence of the string 'second' and then prints part of the element
second_check = [('Second' in line, line[:6]) for line in open('demo.txt')]
print(second_check)
# note how this last example performs mutliple checks enclosed within a tuple.

"""
if Clauses in List Comprehensions
"""
#if clauses can be added to filter out the results of list comprehension
# this only returns a line if is starts with 'S'
start_with_S = [line.rstrip() for line in open('demo.txt') if line[0] == 'S']
print(start_with_S)

"""
Multiple Loops
"""
# list comps can have multiple loops
#this prints all the combinations of combining two elements of two 3 element strings
combo = [x+y for x in 'abc' for y in 'lmn']
print(combo)
# this is the same as writing this:
res =[]
for x in 'abc':
    for y in 'lmn':
        res.append(x+y)
print(res)

#beyond this simple example of multiple loops, it is probably best just to write
# it out in a for loop. List comprehension are meant to be quick and simple
# but if there are too many layers they defeat the purpose

# =============================================================================
#     Tools which utilize iterables
# =============================================================================
# in addition to zip and map, other built in tools make use of the 
# iterable nature of many python objects

"""sorted
"""
# sorted sorts items in an iterable

#to demonstrate this, first create a file that is out of order
F = open('text.txt','w')
F.write('A\n')
F.write('C\n')
F.write('E\n')
F.write('D\n')
F.write('B\n')
F.close()

# then use sorted
out_sorted = sorted(open('text.txt'))
print(out_sorted) # this prints out the sorted output

"""
Zip
"""
# described earlier, zip takes two (or more) iterables and matches elements
# it produces an iterable itself and thus must be wrapped in a list call to print
print(list(zip(open('demo.txt'), sorted(open('text.txt')))))
# in the above line, we zip together the two files. Note the extra values in 
# text.txt are left out. For extra credit, the text.txt file was also sorted.

"""
Enumerate
"""
# as shown earlier, enumerate allows an iterable to be paired with an index value
# just like zip, it must be enclodes in a list call, to print
print(list(enumerate(open('text.txt'))))


"""
Filter
"""
# filter allows items to be selected for which a function is true. 
# more on this will be covered in Chapter 19
print(list(filter(bool, open('text.txt')))) #non-empty = true

"""
More on iteration
"""
#Iteration is a powerful and pervasive tool in python. Many functions and methods
# take into account iteration

#Making a dict from a file's contents
D = {ix: line.rstrip() for (ix,line) in enumerate(open('demo.txt'))}
print(D)

# store a files contents into a list
L = list(open('text.txt'))
print(L)
#note, that the contents can be added to an existing list

L = [1,2,3]
L.extend(open('text.txt')) #append will not work. Extend takes an iterablem append only adds one item
print(L)

#this can also work with slicing
L[1:3] = open('demo.txt') #replaces the 2nd and 3rd elements with the contents of the file
print(L)
#note that if we wanted to just add it after the first element:
L = [1,2,3]
L[1:1] = open('demo.txt')
print(L)

# some other tools:
    # the sum, max, min all work on iterables
L = [1,5,10,-3]
s = sum(L)
print('The sum of list L:\t',L,'\nis:\t',s)

m= max(L)
m_n = min(L)
print('The max of L is:\t',m)
print('The min of L is:\n',m_n)

# the any and all also do:
#any checks to see if "any" element of an iterable is true
print(any(['', 1, 'hi',''])) #this returns true
#all returns if all elements of an iterable are true
print(all(['', 1, 'hi',''])) #this returns false

#note that this means max and min can be used for files too (not just list)

"""
More on Iterables
"""
#as mentioned before, the tools range, map, zip, etc return iterables, which
# require the list function to print.
# this is done purposly, to save memory space. in Python 2x, these returned lists
# which ate memory space and resulted in slower execution.

#some examples of these tools
L = [-1, 2, -3, -3]
#if we want to take the absolute value of all elements in the list, we will
# need to write a loop (the abs function doesn't accept an sequence)
# map will do this too.
absL = map(abs, L) #this creates an iterable.
# it must be enclosed in a list call to print
print(list(absL))

# filter returns the results for which the passed function is true. Here, we are
# checkin to see if the elements are true (an empty element is false)
f = filter(bool, ['spam', '', 'hey', ''])
print(list(f))

# range is slightly different then the others, in that it allows indexing and slicing
# this is not possible with the zip, map, filter, returns.
R = range(9)
print(R[0])
print(R[2])
# it also supports the len() function
print(len(R))


"""
Some stuff with Dictionaries
"""
D = {'a': 1, 'b': 2, 'c': 3}
#dictionaries are iterable too, as are the returns fron D.keys()
print(D.keys()) #note how this isn't a list, but a dict_keys items. That is to save memory

#best way to print a sorted list of keys:
for k in sorted(D):
    print(k, D[k], end=' ')

# lets do that with a long D
D['z'] = 5
D['e'] = 2
D['apple'] = 75
print(D)
for k in sorted(D):
    print(k, D[k], end=', ')

# if I wanted to sort the values, instead of the keys
for k in sorted(D.values()):
    print(k)
    
# the above code prints the sorted values, but information on the key is lost.
# another way of doing this is:
sorted_dict = sorted(D.items(), key=lambda x: x[1])
for k in sorted_dict:
    print(k[1], ' for key ', k[0])
# the code above does a lot.
# D.items() returns a list with 2 element tuples, correspoiinding to (key,val)
# of the dictionary
# the sorted function also excepts an option "key" parameter. This allows the
# sorting to be controlled by a function
# for this we are passing a "lambda" function, which is an inline function declaration
#this one is declaring to sort on the 2nd element, which is the value of the dict
