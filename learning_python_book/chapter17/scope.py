# -*- coding: utf-8 -*-
"""
Created on Sun Oct 25 11:18:03 2020

@author: wwilkinson
"""

X = 99 #this is "global" in scope, meaning throughout this file

def func(Y):
    #local scope
    Z = X+Y # X is a global, and thus the function can use it.
    return Z

print(func(1))
X= 100
print(func(2))

#the example above shows that functions can make use of global scope variables

#Note that global vairable can be overriden in functions. but this  DOES NOT
# OVERWRITE THE GLOBAL VARIABLE.

X = 99 # global

def func2():
    X = 50
    print(X) #prints 50, the local value of X

func2()
print(X) #prints 99, which is the global value of X

"""
Using the Global keyword
"""

#global variables are set in the overarching moduel (i.e. this script for instance).
# they can also be declared in functions, by using the global keyword.

X = 99 # this is a global variable
print(X)

def func3():
    global X #assigns X as a global
    X = 100 # redefines X
    print(X)
func3()
print(X) # this will now print 100, since the global X has been changed

# =============================================================================
# Closures or Factory functions
# =============================================================================
#Closures or factory functions are function objects that are created by other factory objects
# python remembers the state of variables created in these higher level functions.
# as a result, it is a handy way to retain state of various processes.



"""
Using the Non-local keyword
"""
# this is similar to the global statement, but only refers to variables 
# within a function. This allows a function, nested within the higher level
# function to alter a variable, in that higher level function

#Note that the variable must already exist.

def tester(start):
    state = start # referenceing nonlocal works normally
    def nested(label):
        print(label,state) #remembers state in enclosing scope
    return nested
    
# when calling tester, the a function object is created. The function will print
# out what is being passed to it, also with the value passed to the tester function initially
# in this case, that is a 0
F  = tester(0)
F('spam')
F('ham')

# in the next example, by declaring the "state" variable as non-local, it can be altered
# by the enclosed function. Thus we are not stuck with that fixed zero

def tester2(start):
    state = start  # each call gets its own state
    def nested2(label):
        nonlocal state # remembers state in enclosing scope
        print(label, state)
        state += 1  #allows change, since it is nonlocal
    return nested2

G = tester2(0)
G('spam')
G('ham')
G('eggs')

# in the above example, a function object (nested2) is created. the value state
# is created and assiged by the higher level function (tester2), however nested2
# retains this information. What is more, since state (or start) is defined as 
# nonlocal, the lowerlevel function (nested2) has the abiloty to change it.
# inthis way, it increments each time the function is called.

#just playing with this somemore.
#Yes, the lower function does inherit the high level variable (in this case var1)
# it is much like the way global functions are handles.
# a global variable is seen by a function, which can use it, but the function cannot alter that value
# by this I mean it can change that variable in its own local scope, this just will not filter back up to the originating # function
def outer(outer_input):
    print('This is the first level function')
    print('It defines a varaible, var1, with value of 1')
    var1 = 1
    def inner(inner_input):
        print('This is the inner function')
        print('It has been passed variable', inner_input)
        print('Does it know about the higher level value?')
        print(var1)
    return inner

G = outer(5)
G(2)

"""
Using Function Attributes to Retain State
"""
#Another way to retain, and then modify state information without using the non-local
# stuff shown above, is to use function attributes

# in this example, tester3 creates and returns the function nested3. Tester3 also
# initilizes the state value.
# A couple things are going on
    # Since the function nested3 is local within tester3, this means nested3 can see it (see the above example)
    # Also, since nested3.state is just an attribute to a function, this can be altered within the lower level function
        #this is like a work-around to needing to make the non-local call. Changing an object in place, 
        #which is what this is doing, is not an assignment, only changing part of the object. Since it isn't an
        #assignment, no non-local declaration is needed.
# function attributes are supposed to be covered more in chapter 19.
#unlike non-local, this is supported in Python 2 and 3
def tester3(start):
    print('this is the outer function')
    def nested3(label):
        print('This is the inner function')
        print(label, nested3.state)
        nested3.state += 1
    nested3.state =start  #defines initial state, after function is defined
    return nested3

F = tester3(5)
F('ham')
F('spam')

G = tester3(2)
G('pickles')
G('special sauce')

"""
Testing the mutability of lists
"""
#it is odd. Lists are mutabile in all scopes, or so it seems.
# in the below code, an outer function declares a list. 
# the inner function then changes one element. THis change ripples through to the
# outer function.
#So lists are a way to alter varaibles at different levels of scope.

def outer():
    listA = [1,2,3]
    var1 = 5
    print('This is the outer function, and defines a variable, listA')
    print(listA)
    print('It also declares a single variable, var1')
    print(var1)
    def inner():
        print('This is the inner function')
        print('It can see ListA')
        print(listA)
        print('But can I change it')
        listA[2] = 'c'
        print('This is the new listA, as seen by the inner function:')
        print(listA)
        print('The inner function can also see var1:',var1)
        print('However it is unable to change it')
        print('In fact, if I enter code to change it, python breaks')
        print('This is becuase, by assigning a new name of Var1 in this fucntion')
        print('I am changing the variable references, essentially telling this inner function not to see the outer function')
    inner() #calls the lower function
    print('Now, what does listA look like to the outer function?')
    print(listA)

outer()

# some more list mutability stuff

A = [1,2,3,4]
def func(A):
    print('This can see list A:', A)
    print('Lets change it')
    A.append(5)
    print(A)
    print('THis works, by appending A')
    print('But note, the same does not apply if I reassign it by doing:')
    print('A = A + [5]')
    print('The "A within the function" is: ')
    print(A)

func(A)
print('The "higher level A" is: ')
print(A)

# in this example, the two lists are handles separatley. 
# Since A is assigned in the function, this inner A does not alter the global A
# The reason for this difference:
    # The above funtion (func) does a change in place (by calling the .append method)
    # this is not a reassignment and thus doesn't change the name or the scope
    # The function below does an assignment. Thus when python looks up the scopeof
    # variable A, it sees it assigned in the function and does not look at any higher level
    # as a result, A in the function is different than A outside the function.
A = [1,2,3,4]
def func2(A):
    A = A + [5]
    print(A)
func2(A)
print(A)

# =============================================================================
# Chapter Quiz
# =============================================================================

"1"
#What is the output of the following code?
X = 'spam'
def func():
    print(X)
    
func()
# the function above will print "spam" since that is in global scope, and X is not
# assigned within the local scope

"2"
#What is the output of the following code?
X = 'spam'
def func():
    X = 'Ni'
func()
print(X)
#this code will print 'spam' since X is reassignedin the local scope, but does not impact
# the global scope

"3"
# What is the output of the following code?
X = 'spam'
def func():
    X = 'Ni'
    print(X)
func()
print(X)
# this will first print "Ni" then "spam"

"4"
#What is the output of the following code?
X = 'spam'
def func():
    global X
    X = 'Ni'
func()
print(X)
#this will now print 'Ni" since the variable X has been redefined as a global, allowing
# the function to alter the higher level value

"5"
# What is the output of the following code?
X = 'spam'
def func():
    X = 'Ni'
    def nested():
        print(X)
    nested()

func()
print(X)

#this code will first print 'Ni'. This is the value seen by the inner function (nested)
# then the value "spam" will be printed, since the global variable X has not been changed.

"6"
# What is the output of the following code?
def func():
    X = 'Ni'
    def nested():
        nonlocal X
        X = 'spam'
    nested()
    print(X)

func()
#this will print 'spam'. As the inner function alters the value seen in the upper function

