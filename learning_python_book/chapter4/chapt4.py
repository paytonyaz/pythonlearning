# -*- coding: utf-8 -*-
"""
Created on Fri Jun 26 08:08:10 2020

@author: wwilkinson
"""


S = 'Spam!';

""" praticing slicing
"""

print(S[0])  #first entry'
print(S[-1]) #last entry
print(S[1:3]) # up to (not including) 3
print(S[1:])  #to the end
print(S[:3])  #from the begining
print(S[:-1])  #verything but the end


"""
Testing concatenation
"""
SS = S + 'xyz';
print(SS)

print(S*8) # string can be repeated too

"""
Testing out lists
"""
S = "shrubbery";
L = list(S); # this creates a list of the string S, with each letter being an element
# Nice thing about lists is each individual part can be changed, unlike strinkg

# S[0] = 'X';  #this is not allowed
L[0] = 'X'; # this is though

# lists can be joined back together
SS = ''.join(L) # this is is a join command with an empy deliminter
print(SS)  


# there is a weird thing called "byte array" which allows some strings to be changed
B = bytearray(b'spam') # creates a bytearray string
B.extend(b'eggs') # adds to it\
print(B.decode()) # this converts B to a normal string to be printed

# =============================================================================
# Playing with strings
# =============================================================================
S = 'Spam';
pa_loc = S.find('pa') # finds offset of the searched string. returns -1 if nothing is found
print(pa_loc)
print(S.replace('pa', 'XYZ')) # replaces the keyword

line = 'aaa,bbb,ccc'
line_delim = line.split(',')  #splits the string using a deliminter

line2 = 'aaa,bbb,ccc,dd\n'
line2.rstrip() # this removes whitsepace on the right side
print(line2.rstrip().split(','))  # this combines the two commands and then prints the newly created list

""" 
wierd string substituion
"""
Ls = '%s, eggs and %s' % ('spam','SPAM')  # first method of string substituion
print(Ls)

Ls2 = '{0}, eggs, and {1}'.format('spam', 'SPAM') # second method of string sub

print(Ls2)

# =============================================================================
# working with Lists 
# =============================================================================

L = [123, 'spam', 1.23];
print(L)

print(L[:-1]) # slices the list
print('Length of the list is: ', len(L)) 
L.append('NI') #adds to the list
print('The lenght of the list is now:',len(L))
print(L)

# the list can be shrunk too
print('Pops out the third element of the list',L.pop(2))
print('The list is now', len(L), 'elements long')
print(L)


M = ['aa', 'dd', 'cc', 'bb']
print('The ''M'' list consits of:')
print(M)
print('No it will be sorted')
M.sort()
print(M)

"""
Nested Lists
"""
M = [[1,2,3], [4, 5, 6], [7, 8, 9]]
print('M now equals:')
print(M)
print('A nested List')

""" 
List comprehension
"""
secondCol = [row[1] for row in M ] # this grabs the second element of each row in the matrix
aa = [M[i][1] for i in [0,1,2]]  #this does the same thing as above, just a different way. Now the incrementer is more explicit

diagVal = [M[i][i] for i in [0,1,2]] #this creates a list containing the "diag" values of the matrix M

# range creates succsive integers. It requires the list fcuntion to print the value out
list(range(4))
#can also set start, stop and step sizes
list(range(-5, 10))
list(range(-5,10,2))

# uses list comprehension and range to create an aray fo squares and cubes
square_and_cube = [[x ** 2, x ** 3] for x in range(4)]
print(square_and_cube)

# lists, sets, and dictionaries all allow list comprehension

# lists
uniCode_val = [ord(x) for x in 'spaam']
print(uniCode_val)

# sets
uniCode_val = {ord(x) for x in 'spaam'}
print(uniCode_val)  # sets removes duplicates


# dict
uniCode_val = {x: ord(x) for x in 'spaam'}
print(uniCode_val) # dictionary keys are unique


# =============================================================================
# Generators (sneak peak)
# =============================================================================
# by adding a parenthesis around list comprehension, a generator is created
G = (sum(row) for row in M) # greates a generator of row sums
print(next(G)) #iterates through the generator
print(next(G)) # iterates one more time

# =============================================================================
# Dictionaries
# =============================================================================
D = {'food': 'Spam', 'quantity': 4, 'color': 'pink'} # creates a dictionary

print(D['food']) # the dictionary is indexed with the key
print(D)

# many times a dictionary is built up as the code runs, like so
D = {} # makes an empy dict
#then the dictionary is built up later
D['name'] = 'Bill';
D['job'] = 'Engineer'

print(D)

#dictionaries can also be created with the "dict" call
bob1 = dict(name='bob', job = 'dev', age=40)
print(bob1)

#Nesting dicts
rec = {'name': {'first': 'Bob', 'last': 'Smith'}, 
       'Jobs': ['dev', 'mgr'],
       'age': 40}
print(rec['name']) # prints nested dict for the name
print(rec['name']['first']) # indexes into the 2nd dict to print only the first name

print(rec['Jobs'])

# dictionaries can be added to
rec['Jobs'].append('janitor')
print(rec['Jobs'])

# dictionaries allow one to check tha validity of a key
not_a_key = 'f' in D
print(not_a_key)

"""
Taking into acount bad keys
"""
# Asks user input, to select a key within a dictionary. The value for that key will be printed
print('Dictionary D is:', D)
key = input('Enter a Key for Dictionary D: ')

if not key in D:
    print('The key you entered does not exist.')
else:
    print(D[key])

# another way to do this is with the "get" fucntion, which returns a default value, if it failes
value = D.get('f',-1) # this tries to get the "f" key from the dictionary. The 2nd argument is the default return value
print(value) # return of the deffault value (in this case -1) means there was no key in the dict

"""
Sorting keys
"""
D = {'a': 1, 'c': 3, 'b': 2,'d': 4}
Ks = list(D.keys())
print('Ks = ', Ks)
#now to sort Ks
Ks.sort()
print('Now that Ks is sorted it is:', Ks)

# now iterates through the dict
for key_val in Ks:
    print(key_val, '=>', D[key_val])

# This can also be done with the "sorted" fucntion
print('The D dictionary is: ', D)
print('This will print it in a sorted manner')
for key_val in sorted(D):
    print(key_val, '=>', D[key_val])

# =============================================================================
#Loops 
# =============================================================================
#loops through a string and prints out the capitalized version
for c in 'spam':
    print(c.upper())

# the for loop is specifically built for stepping through sequencies. The While loop is more general

x = 4
while x > 0:
    print('spam!' * x)
    x -= 1 #subtracts one from X


# =============================================================================
# Tupples
# =============================================================================

T = (1,2,3,4)
print('A tuple, T, was created', T)
print('The index value corrsponding to value 4 is: ' ,T.index(4))
T = T+(5,6); # tupples can be added to
print(T)
# big thing about tuples is the elements cannot be changed once created. This 
# can be handy in large programs

# =============================================================================
# File operations
# =============================================================================
f = open('data.txt','w') #opens a file, to write to it (the 'w')
f.write('Hello\n')
f.write('World\n')
f.close()

# now reads from the file
f = open('data.txt') # reading from a file is default
text = f.read()
print(text)
f.close()

