# -*- coding: utf-8 -*-
"""
Created on Mon Feb 22 09:27:16 2021

@author: wwilkinson
"""

class Point:
    """
    Define a structure with the fields 'x', 'y', and 'z' for a
    point in a Cartesian coordinate system.
    """
    def __init__(self, x=0, y=0, z=0):
        self.x = x
        self.y = y
        self.z = z

def pointSlopeEq( point1, point2 ):
    """
    This function will calculate the slope of the line between two
    Cartesian coordinates. It will return an function used to
    calculate y-coordinate for a given x-coordinate

    Input:
           point1 - Point class object for the first Cartesian
                    coordinate
           point2 - Point class object for the second Cartesian
                    coordinate

    Output:
           function -
    """

    slope = (point2.y - point1.y) / (point2.x - point1.x)

    def eqFunc(x):
        """
        Create a linear equation function based on the two Cartesian
        coordinates in the pointSlopeEq function.

        The equation y = mx + b can be calculated from the point-slope
        form of an equation given by:

           y - y1 = m * (x - x1) ==> y = m * (x - x1) + y1

        Therefore given the specific slope and a point on that line,
        the linear equation can be derived.

        This function will accept an x-coordinate and return the
        y-coordinate for a given slope and Cartesian coordinate.
        """

        return slope * (x - point1.x) + point1.y

    # Return the eqFunc function in the namespace that the
    # pointSlopeEq function was called
    # NOTE: return reference to function (DO NOT INCLUDE PARENTHESIS)
    return eqFunc

# ------------------------------
# -                            -
# -     Using the Closures     -
# -                            -
# ------------------------------
import matplotlib.pyplot as plt

# f1, f2, and f3 are closures which can be used to calculate the y
# coordinate of a line defined by the two Cartesian coordinate
# points.
f1 = pointSlopeEq( Point(0,0), Point(1,3) )
f2 = pointSlopeEq( Point(0.5,0), Point(5,8) )
f3 = pointSlopeEq( Point(2,10), Point(3,2) )

x = list(range(6))
y1 = []
y2 = []
y3 = []

for xVal in x:
    # The variable f1, f2, f3 are closures which accept an x
    # coordinate argument and then calculate the y coordinate based on
    # slope of the line calculated when the closure was created.
    # Append the calculated y-coordinate to the respective list
    y1.append(f1(xVal))
    y2.append(f2(xVal))
    y3.append(f3(xVal))

# ----------------------------
# -     Plot with Graphs     -
# ----------------------------
plt.figure()
plt.plot(x, y1, 'r',label='y1')
plt.plot(0, 0, 'r*')
plt.plot(1, 3, 'r*')
plt.plot(x, y2, 'g',label='y2')
plt.plot(0.5, 0, 'g*')
plt.plot(5, 8, 'g*')
plt.plot(x, y3, 'b',label='y3')
plt.plot(2, 10, 'b*')
plt.plot(3, 2, 'b*')
plt.legend()
plt.grid(True)

plt.xlabel("X")
plt.ylabel("Y")
plt.title("Using Closure for Linear Equation")

plt.show()
