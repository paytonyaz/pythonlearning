# Shortcuts

## Spyder
* skip to a line number
	* CTRL+L
* Move line down
	* ALT + Down_arrow
* Comment line
	* CTRL+1
		* This toggles
* Block comment
	* CTRL+4
	* CTRL+5 to undo
* Delete whole line
	* CTRL+D
* Move to code editor
	* CTRL+shift+E
* Move to ipython control
	* CTRL+shift+I
* Move to variable view
	* CVTRL+shift+V
* Move to file explorer
	* CTRL+Shift+X
* Move to plots view
	* CTRL+Shift+G

