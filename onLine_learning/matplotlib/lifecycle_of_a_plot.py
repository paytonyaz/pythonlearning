# -*- coding: utf-8 -*-
"""
Created on Sat Jan  2 17:53:33 2021

@author: Bill

https://matplotlib.org/3.3.3/tutorials/introductory/lifecycle.html#sphx-glr-tutorials-introductory-lifecycle-py
"""

import matplotlib.pyplot as plt
import numpy as np

# creates some data to plot
data = {'Barton LLC': 109438.50,
        'Frami, Hills and Schmidt': 103569.59,
        'Fritsch, Russel and Anderson': 112214.71,
        'Jerde-Hilpert': 112591.43,
        'Keeling LLC': 100934.30,
        'Koepp Ltd': 103660.54,
        'Kulas Inc': 137351.96,
        'Trantow-Barrows': 123381.38,
        'White-Trantow': 135841.99,
        'Will LLC': 104437.60}
group_data = list(data.values())
group_names = list(data.keys())
group_mean = np.mean(group_data)

#now to make the figure. Goal is to make a bar plot
fig, ax = plt.subplots()
ax.barh(group_names,group_data)

#controlling the style
#there are many styles avaialbe. Thse can be found with:
print(plt.style.available)

#lets activate a style
plt.style.use('fivethirtyeight')
#now lets remake the plot to see how it looks
fig, ax = plt.subplots()
ax.barh(group_names,group_data)

#now to finetune the plot. 
#many parameters can be set at once, by calling the .setp() method
fig, ax = plt.subplots()
ax.barh(group_names, group_data)
labels = ax.get_xticklabels()
plt.setp(labels, rotation=45, horizontalalignment='right')

# we can also set labels with the .set() method
ax.set(xlim=[-10000, 140000], xlabel='Total Revenue',ylabel='Company Name',
       title='Company Revenue')

