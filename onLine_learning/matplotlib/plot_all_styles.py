# -*- coding: utf-8 -*-
"""
Created on Sat Jan  2 18:24:12 2021

@author: Bill
"""
import matplotlib.pyplot as plt
import matplotlib as mpl


# creates some data to plot
data = {'Barton LLC': 109438.50,
        'Frami, Hills and Schmidt': 103569.59,
        'Fritsch, Russel and Anderson': 112214.71,
        'Jerde-Hilpert': 112591.43,
        'Keeling LLC': 100934.30,
        'Koepp Ltd': 103660.54,
        'Kulas Inc': 137351.96,
        'Trantow-Barrows': 123381.38,
        'White-Trantow': 135841.99,
        'Will LLC': 104437.60}
group_data = list(data.values())
group_names = list(data.keys())


#lists all availble styles
avail_styles = plt.style.available




dir_location = 'fig_styles/'

#ss = avail_styles[1]
for ss in avail_styles:
    plt.style.use(ss)
    #this ensures all objects printed to the figure are shown (prevents cutting out names)
    plt.rcParams.update({'figure.autolayout': True}) #note. this has be done everytime, which is why it is in the loop
    fig, ax = plt.subplots()
    ax.barh(group_names,group_data)
    ax.set_title(ss)
    ax.set_xlabel('Revenue')
    #ax.set_ylabel('Company Name')
    fig.show()
    fig.savefig(dir_location+ss)
    plt.close()


