# -*- coding: utf-8 -*-
"""
Created on Sat Jan  2 12:16:23 2021

@author: Bill

This is an introduction to Matplotlib, from:
https://matplotlib.org/3.3.3/tutorials/introductory/usage.html#sphx-glr-tutorials-introductory-usage-py

Note: The shortcut to jump to the plots pannel is:
    ctrl+shift+G
"""

import matplotlib.pyplot as plt
import numpy as np


fig, ax = plt.subplots() #creates a figure containg a single axes.
ax.plot([1,2,3,4],[1,4,2,3])

#Note, we do not necessarily need to explictly create the axes. we could also do:
plt.plot([1,2,3,4],[1,4,2,3]) #this plots the same thing


# =============================================================================
# Creating figures and different axes
# =============================================================================
#one can create a figure with
fig = plt.figure() #an emptry figure with no axes
fig, ax = plt.subplots()  #a figure with a single axes
fig, axs = plt.subplots(2,2) # a figure with a 2x2 grid of Axes

# =============================================================================
# Different styles of plotting
# =============================================================================
# there are two general ways of plotting.

# 1. The Object Orientted (OO) way, were axes and figures are created and then
# methods within matplotlib are called to operate on these axes

# 2. The Function style (or Pyplot style). Where one relys on Pyplot to automatically 
# create the and manage figures and axes. Then functions are called for plotting

# the matplotlib user guide recommends using the OO style for scripts that will
# be used again and again. the pyplot style is useful for inline commands
#such as when typing in the console or in a jupyter notebook

"""
The OO Style:
"""
x = np.linspace(0,2,100) # creates a linear set of values

#then plots stuff
fig, ax = plt.subplots() #creates a figure and axes
ax.plot(x,x,label='linear') # plots the same data on both axis
ax.plot(x,x**2,label='quadratic') # plots x squared
ax.plot(x,x**3,label='cubic') #plots x cubed
ax.set_xlabel('x label')
ax.set_ylabel('y label')
ax.set_title('Example Plot')
ax.legend() #add a legend. This will make use of the labels called earlier.


"""
The Pyplot style
"""
plt.plot(x,x,label='linear')
plt.plot(x,x**2,label='quadratic')
plt.plot(x,x**3,label='cubic')
plt.xlabel('x axis')
plt.ylabel('y axis')
plt.title('Example Plot')
plt.legend()

# =============================================================================
#  Using function to help plotting
# =============================================================================

#often times the same type of plot needs to be made, while only varying the 
# dataset. When doing this it is often useful to creat simple functions
# for example

def my_plotter(ax, xdata, ydata, param_dict):
    """
    
    
    Parameters
    ----------
    ax : axes object to which the data will be plotted.
    xdata : x axis data.
    ydata : y-axis data
    param_dict : dict
                dictionary of kwargs to pass to ax.plot
    
    Returns
    out : list
        list of artists added
    
    """
    out = ax.plot(xdata, ydata, **param_dict)
    return out

#now to test out this fucntion
data1, data2, data3, data4 = np.random.randn(4,100) #creates random dist
fig, ax = plt.subplots(1,1) #creates a figure and axes
my_plotter(ax, data1, data2, {'marker':'x'})  #calls function using ''x'' as a marker

#or if we wanted 2 subplots
fig, (ax1,ax2) = plt.subplots(1,2) #creates a figure and 2 axes objects
my_plotter(ax1,data1,data2,{'marker':'x'})
my_plotter(ax2,data3, data4,{'marker':'o'})

#to save this figure to a pdf. do the following
plt.savefig('testSave.pdf',format='pdf')
