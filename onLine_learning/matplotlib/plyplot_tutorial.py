# -*- coding: utf-8 -*-
"""
Created on Sat Jan  2 16:59:04 2021

@author: Bill

https://matplotlib.org/3.3.3/tutorials/introductory/pyplot.html#sphx-glr-tutorials-introductory-pyplot-py
"""
# =============================================================================
#  Making a Scatter plot
# =============================================================================

import matplotlib.pyplot as plt
import numpy as np

#creates a data dict from which the plot will be derived
data = {'a': np.arange(50),
        'c': np.random.randint(0,50,50),
        'd': np.random.randn(50)}
data['b'] = data['a'] + 10 * np.random.randn(50) #adds a random offset to values 1-50
data['d'] =  np.abs(data['d'])*100 

plt.scatter('a','b',s='d',c='c',data=data) #plots the a and b variables of the data dict
# the s parameter indicates the size of the points 
# the c parameter is color.
#note that the call to scatter refernces the data dictionary 'data'.

plt.xlabel('Entry A')
plt.ylabel('Entry B')
plt.show() 

# playing with colors. What happen if I leave off the "c" parameter
plt.scatter('a','b',s='d',data=data) #plots the a and b variables of the data dict
#every circle is the same color.which makes sense.

#what happens if I set C to a, which is linearly increasing from 0-49
plt.scatter('a','b',s='d',c='a',data=data) #plots the a and b variables of the data dict
#color starts at a dark purple, and goes to yellow.

#what happens if I make it go from 0-255?
data['c'] = np.linspace(0,255,50,dtype=int)
plt.scatter('a','b',s='d',c='c',data=data) #plots the a and b variables of the data dict
#this produces the same thing a going from 0-50, impling the absolute number may not matter. Weird
