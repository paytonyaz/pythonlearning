# -*- coding: utf-8 -*-
"""
Created on Wed Oct 28 20:05:42 2020

@author: wwilkinson
video from here: https://www.youtube.com/watch?v=qErBw-R2Ybk&list=PL55RiY5tL51o5jBXR1h2JvFm0L-fbThG4&index=3
Additional information on pyplot here:
    https://matplotlib.org/tutorials/introductory/pyplot.html#sphx-glr-tutorials-introductory-pyplot-py

"""

import matplotlib.pyplot as plt

apl_price = [93.95, 112.15, 104.05, 144.85, 169.49]
msft_price = [39.01, 50.29, 57.05, 69.98, 94.39]
year = [2014, 2015, 2016, 2017, 2018]

plt.plot(year, apl_price,'--*k')
plt.plot(year, msft_price,'-*g')
plt.xlabel('Year')
plt.ylabel('Stock Price')
plt.show()



