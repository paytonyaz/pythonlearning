# -*- coding: utf-8 -*-
"""
Created on Tue Jun 23 12:34:07 2020

@author: wwilkinson
"""

user_name_list = ['Bill'];

user_name_tuple = ('Bill');

# prints out the type of the variables
print(type(user_name_list))
print(type(user_name_tuple))

#when making a single element, tuple, it actually isn't a tuple

user_name_list_second = ['Bill','Andrew','Dominic']
user_name_tuple_second = ('Bill','Andrew','Dominic')

print(user_name_list_second)
print(user_name_tuple_second)

# we can access both by indexing

print(user_name_list_second[0])
print(user_name_tuple_second[0])

#can also "slice"
print(user_name_list_second[0:2])
print(user_name_tuple_second[0:2])

# but tuples cannot be changed, like lists
user_name_list_second[1] = 'Sam'  #changes the second index of the list
# user_name_tuple_second[1] = 'Same' #this will fail


# returns the index for the value 'Dominic"
user_name_tuple_second.index('Dominic')

# =============================================================================
# But why do we want tupples?  
# =============================================================================

# it is because we can "unpack" them easily, used for muliple assignments

user_data = ('Bill', 35, 'Male');
# now look at multiple assignments 
name, age, gender = user_data; # this assigns variable names to the values in the tuple.
print(name)
print(gender)

# but what if there are more values in the tuple?
user_data = ('Bill', 35, 'Male', 'Baseball', 'United States');
# the value with the "*" now contains the extra tupple values
name, age, gender, *other = user_data; 
print(other)


# =============================================================================
# Lets make something with this feature
# =============================================================================
# this will prompt the user to enter information and save it into a tupple

def get_user_data():
    user_email = input('Enter your Email: ')
    user_password = input('Enter your Password: ')
    
    # returns value in a tupple
    return (user_email, user_password)
    
signup_data = get_user_data()
#tuple unpacking 
email, password = signup_data

print(signup_data)

    