# -*- coding: utf-8 -*-
"""
Created on Thu Jul  9 07:38:56 2020

@author: wwilkinson
"""

"""
Video from: https://www.youtube.com/watch?v=oQfNYqz8pLs&list=PL55RiY5tL51o5jBXR1h2JvFm0L-fbThG4&index=2
"""

import json

with open('./source/source-data.json') as access_json:
    read_content = json.load(access_json)
    
# prints the type of the output... it ends up being a dict
print(type(read_content))

# we can access values of the dictionary by using the key
question_access = read_content['results'] # results is a keyin the dict

print(type(question_access))  # prints out the type. It is a list, since we accessed the data within the dict

print(question_access[0]) # prints the first list item. This is actually a dictionary, within the list

# how to we access all the elements of the list? We can use a for loop
for question_element in question_access:
    print(question_element)

# prints out the type, it is a dictionary, because we accessed all the elements    
print(type(question_element))

