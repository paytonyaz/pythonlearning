# -*- coding: utf-8 -*-
"""
Created on Wed Jul  8 07:53:57 2020

@author: wwilkinson
"""

"""
Vido from: https://www.youtube.com/watch?v=E1ZAVEmRwyI
"""

# =============================================================================
# multiply a list by a scalar
# =============================================================================
my_list = [1,2,3];
# in Matlab the following code would multiply the list by 2... but in Python that doesn't happen
print(2*my_list)

# below is a method for doing this. Loop through the list, and create a seperate list with the multiplied vlaues
multiply_vals = []
for item in my_list:
    multiply_vals.append(item*2 )

print(multiply_vals)
    
# but there is another way of doing this, using list comprehension
multiply_vals2 = [item * 2 for item in my_list]
    
# =============================================================================
# Dictionaries 
# =============================================================================

users = [{'name': 'Bill', 'age': 35}, {'name': 'Max','age': 30}, {'name': 'Dirk', 'age': 38}]

user_name = [user_element['name'] for user_element in users ]
print(user_name)

# lets add some filter condition. Find onyl ages over 30
user_name = [user_element['name'] for user_element in users if user_element['age'] > 30 ]
print(user_name)

# now lets make a nested for loop
user_groups = [
    [
         {'name':'Bill', 'age': 35},
         {'name': 'Max', 'age': 30}
     
     ],
    [
     {'name': 'Sarah','age': 30},
     {'name': 'Julie','age': 32}
     ]
]

# this uses two for loops, to go through the two lists
user_name = [person['name'] for group in user_groups for person in group]
print(user_name)


