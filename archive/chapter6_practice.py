# -*- coding: utf-8 -*-
"""
Created on Sat Jan 19 15:10:38 2019

@author: Bill
"""

location = "http://greenteapress.com/thinkpython/html/thinkpython007.html"

## Exercise 1
# write a comapre function which returns 1 if x > y  or returns 0, if x ==y and -1 if x < y

def compare(x, y):
    if x > y:
        return 1
    elif (x == y):
        return 0
    else:
        return -1

outputVal = compare(1,1);
print(outputVal)


## factorials

def factorial(n):
    if (n == 0):
        return 1
    else:
        tempVal = factorial(n-1)
        result = tempVal*n
        return result
    
## fibonacci
# output of fibonacci(n) is fibonacci(n-1) + fibonacci(n-2)
# fibonacci(0) = 0
# fibonacci(1) = 1
# this first function does it recurssively
def fibo(n):
    if (n ==0):
        return 0
    elif (n==1):
        return 1
    else: 
        tempVal = fibo(n-1) + fibo(n-2)
        return tempVal

# this second function does it non-recursivley
def fibo_loop(n):
    if isinstance(n,int):
        print("Input is not an integer")
        return None
    elif n<0:
        print("Input Value is less than 0")
        return None
    elif n == 0:
        return 0
    elif n==1:
        return 1
    else:
        f1 = 0
        f2 = 1
        for ii in range(1,n):
            temp  = f1 + f2
            f1 = f2
            f2 = temp
        return temp
    
## Ecercise 7
def is_power(a, b):
    
        
        
    
    
    
    