# -*- coding: utf-8 -*-
"""
Created on Sun Apr 21 14:32:32 2019

@author: Bill
"""

location = "http://scipy-lectures.org/intro/matplotlib/matplotlib.html"

from matplotlib import pyplot as plt
import numpy as np

X = np.linspace(-np.pi, np.pi, 256,endpoint=True)
C, S = np.cos(X), np.sin(X)

plt.plot(X,C,label="cosine")
plt.plot(X,S,label="sine")
plt.legend()
#plt.show()
