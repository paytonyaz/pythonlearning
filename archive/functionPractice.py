# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""
import math

print(math.pi)
location = "http://greenteapress.com/thinkpython/html/thinkpython004.html"

def print_lyrics():
    print("You know me, still the same old G")
    print("But I been low key")
    
    
def repeat_lyrics():
    print_lyrics()
    print_lyrics()
    
def right_justify(s):
    #goal it to take in a string and shift it to the right such that the last character is 70 chars to the right
    inputLength = len(s)
    if inputLength < 70:
        blankDistance = 70-inputLength
        blank = " "
        print(blank*blankDistance+s)
    else:
        print(s)
        
def perform_multiple(f,x):
    # calls a function twice, passing it the x variable.
    print(f(x))
    print(f(x))
    print(f(x))
    #this is a comment!    
    
def printGrid(row, col):
    width = 4 #width of each column
    height = 4 # heightof each row
    
    #defines top line of the entire grid
    topMarker = "-"
    topString = "+" + width*topMarker
    topString = col*topString+"+"
    
    #defines side of each column
    colMarker = "/"
    colFiller = " "
    colString = colMarker+width*colFiller
    colString = (col+1)*colString
    
    # prints the grid
    for rr in range(1, row+1):
        print(topString)
        for hh in range(1,height+1):
            print(colString)
            
    #prints the bottom line
    print(topString)
        
### Main ####     
string1 = "H"
right_justify(string1*50)
perform_multiple(math.sin,1)
#repeat_lyrics()
#repeat_lyrics()

## practice printing variables
aa = 5
print("this is a variable ", aa, "sweet huh?")

## This is practice with a for loop
for ii in range(1,5):
    print(ii)
    
## this calls the grid print function
printGrid(2,2)
print("that was a 2x2 grid")
print("now time for a 4x4")
printGrid(4,4)
print("what about a 5x3?")
printGrid(5,3)
print()
print("Now let's see you enter some values:")
rowVal = input("Enter the row value:")
colVal = input("Enter the column value:")
print()
print("Now generating a", rowVal, "by", colVal, "grid")
printGrid(int(rowVal),int(colVal))
