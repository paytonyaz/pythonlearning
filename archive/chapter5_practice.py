# -*- coding: utf-8 -*-
"""
Created on Thu Jan  3 18:48:30 2019

@author: Bill
"""
location = "http://greenteapress.com/thinkpython/html/thinkpython006.html"
import math

## Work with the modulus operator ##
# the modulus operator can be used to obtain the right most digit, in base ten.
# this is done by performing a %10
inputVal = input("Enter an integer number of at least 2 digits: ")
print()
print("The right most value of this number is", int(inputVal)%10 )

# similarly modulus 100 will obtain the last 2 digits
print()
inputVal = input("Enter an integer number of at least 3 digits: ")
print()
print("The right two most numbers are", int(inputVal)%100)

## practicing recursion ###

def countdown(n):
    """ first example of recursion. This function will take in an integer number
    and then decrease the number until 0, at which time Blastoff! will be printed
    """
    if n <= 0:
        print("Blastoff!")
    else:
        print(n)
        countdown(n-1)

countdown(10)

### Exercise 2: ###
def do_n(f, n):
    if n <= 0:
        return
    f()
    do_n(f,n-1)
    
def printWords():
    print("My candle burns at both ends")
    print("It will not last the night")
    print("But oh my foes and ahh my friends")
    print("It makes a lovely light\n")
        
do_n(printWords,3)

### Exercise 3 ###

def check_fermat(a,b,c,n):
    print("Checking to see if ", a, "^", n, "+", b, "^", n,"=",c, "^",n, sep="")
    equationOutput = math.pow(a,n) + math.pow(b,n)
    print("Output of equation is", equationOutput)
    if (equationOutput == math.pow(c,n)) and (n > 2):
        print("Wow, Fermat was wrong?\n")
    else:
        print("Nope, he was right\n")

check_fermat(3,4,5,3)
check_fermat(3,4,5,2)

### Excercise 4 ###

def is_triangle():
    seg1 = int(input("Enter length of first segment:\n"))
    seg2 = int(input("Enter length of the second segment:\n"))
    seg3 = int(input("Enter length of the third segment:\n"))
    
    if seg1+seg2 < seg3:
        print("can't be a triangle")
    elif seg1+seg3 < seg2:
        print("Can't be a triangle")
    elif seg2 + seg3 < seg1:
        print("Can't be a triangle")
    else:
        print("Valid Triangle Inputs")
        
is_triangle()